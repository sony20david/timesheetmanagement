import { response } from "express";
import { request } from "express";
import {
  updating_hr_password,
  updating_employee_password,
} from "../utilities/authentication_validation.js";

/**
 * @path /forget/user_type is the path by using this path we can validate the user data and update the user/admin password
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 */
const forgot_user_handler = (request, response) => {
  // login_data is the variable that contains the request body data
  const forget_data = request.body;
  // initializing the count is 0 because we need to validate the request parameters
  // geting user_type from request params
  //const user_type = request.params.user_type;
  /*
      we need only username and password for user login
              if any extra params are pass in the request it throws error */
  // if parameter count are valid execution goes through the else block
  if (forget_data.position === "employee") {
    updating_employee_password(forget_data, response);
  }
  //if not it will updates the hr password function into the database...
  else {
    updating_hr_password(forget_data, response);
  }
};

//exporting this func to the server
export { forgot_user_handler };
