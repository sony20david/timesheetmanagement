import jwt from "jsonwebtoken";
import dotenv from "dotenv";
dotenv.config();
import { request, response } from "express";
// importing internal modules models from model.js
//importing internal modules models from authentication_validation.js
import {
  inserting_user_register_data_into_database,
  inserting_hr_register_data_into_database,
  pushing_data_time_sheet,
} from "../utilities/model.js";
import {
  validate_user_login_data,
  validate_admin_login_data,
  validate_hr_login_data,
} from "../utilities/authentication_validation.js";

/**
 * @path /login is the path by using this path we can post the user login data and validate it using the register data
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 */
const login_user_handler = (request, response) => {
  // login_data is the variable that contains the request body data
  const login_data = request.body;
  const user = { name: login_data.username };
  const jwt_token = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET);
  login_data.jwt_token = jwt_token;
  // initializing the count is 0 because we need to validate the request parameters
  var count = 0;
  // iteraing over the body data because we need to count the body params
  for (let item in login_data) {
    // updating the count
    count = count + 1;
  }
  // geting user_type from request params
  const user_type = request.params.user_type;
  /*
          we need only username and password for user login
                  if any extra params are pass in the request it throws error */
  if (user_type === "user") {
    login_data.role = "employee";
    validate_user_login_data(login_data, response);
  } else if (user_type === "admin") {
    login_data.role = "admin";
    validate_admin_login_data(login_data, response);
  } else {
    login_data.role = "hr";
    validate_hr_login_data(login_data, response);
  }
};

/**
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 * create_profile_path_handler is an asynchronus function checks which type of user to be inserted
 * if user_type is employee then  goes to inserting_user_register_data_into_database function
 * else goes to inserting_hr_regsiter_data_into_database
 */
const create_profile_path_handler = (request, response) => {
  const register_data = request.body;
  // if returned result doesn't contains 200 status code the post method returns the error response

  if (register_data.position === "employee") {
    inserting_user_register_data_into_database(register_data);
    response.send(
      JSON.stringify({ status_code: 200, status_message: "success" })
    );
  }
  //if not satisfies it will push into register database...
  else {
    inserting_hr_register_data_into_database(register_data);
    response.send(
      JSON.stringify({ status_code: 200, status_message: "success" })
    );
  }
};
/**
 *
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 * checks for the params and inserts the data
 */
const Fill_timesheet_data_handler = (request, response) => {
  const timesheet_data = request.body;
  pushing_data_time_sheet(timesheet_data, response);
  // response.send("sucess");
};

//exporting functions into the server
export {
  login_user_handler,
  create_profile_path_handler,
  Fill_timesheet_data_handler,
};
