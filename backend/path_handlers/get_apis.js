import { request, response } from "express";
import {
  CREATE_EMPLOYEE_DETAILS_DATA,
  FILL_TIME_SHEET_DATA,
  CREATE_HR_REGISTER_DATA,
} from "../utilities/model.js";
/**
 *
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 * function for getting the details of employee from the database
 * if not present print the error
 */
const get_profile_details_handler = (request, response) => {
  //fetching get_profils details for getting the all employee details
  CREATE_EMPLOYEE_DETAILS_DATA.find({}, (err, data) => {
    // any error occur in the data fetching it prints in the console
    if (err) {
      // printing error in console
      console.log(err);
    } //if there is no error in the data fetching it goes through the else block
    else {
      //it sends the data
      response.send(data);
    }
  });
};

/**
 *
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * @param response is the callback parameter that sends the data based on the request data
 * gets all the fill_timeheets data if it finds in the database
 */
const get_fill_timesheets_data = (request, response) => {
  FILL_TIME_SHEET_DATA.find({}, (err, data) => {
    // any error occur in the data fetching it prints in the console
    if (err) {
      // printing error in console
      console.log(err);
    }
    //if there is no error in the data fetching it goes through the else block
    else {
      response.send(data);
    }
  });
};

/**
 *
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * @param response is the callback parameter that sends the data based on the request data
 * checks database based on empid if presents then sends data otherwis
 * throws an error
 */
const get_specific_timesheets = (request, response) => {
  //initializing empid as request.params.empid
  let empid = request.params.empid;
  //changing the datatype into int
  empid = parseInt(empid);
  FILL_TIME_SHEET_DATA.find({ empid: empid }, (err, data) => {
    // any error occur in the data fetching it prints in the console
    if (err) {
      // printing error in console
      console.log(err);
    }
    //if there is no error in the data fetching it goes through the else block
    else {
      response.send(data);
    }
  });
};

const getting_profile_data = (request, response) => {
  //here role defines role from request params
  const role = request.params.role;
  //here empid defines empid from rq params
  let empid = request.params.empid;
  empid = parseInt(empid);
  if (role === "employee") {
    get_emp_profile_by_empid(response, empid);
  } else {
    get_hr_profile_by_empid(response, empid);
  }
};

/**
 *
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 * getting data from the database based on the employee id if present sends the data
 * if not present in the database prints error message
 */
const get_emp_profile_by_empid = (response, empid) => {
  CREATE_EMPLOYEE_DETAILS_DATA.find({ empid: empid }, (err, data) => {
    if (err) {
      console.log(err);
    } else {
      //it sends data as response...
      response.send(JSON.stringify(data));
    }
  });
};

/**
 *
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 * getting data from the database based on the employee id if present sends the data
 * if not present in the database prints error message
 */
const get_hr_profile_by_empid = (response, empid) => {
  CREATE_HR_REGISTER_DATA.find({ empid: empid }, (err, data) => {
    if (err) {
      console.log(err);
    } else {
      response.send(JSON.stringify(data));
    }
  });
};

//exports function get_profile_details_handler and get_emp_profile_by_empid
export {
  get_profile_details_handler,
  getting_profile_data,
  get_fill_timesheets_data,
  get_specific_timesheets,
};
