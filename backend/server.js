// importing express from the express module
import express from "express";
//importign bodyparser from the body-parser module
import bodyParser from "body-parser";
//Creating an instance of express
const app = express();
//Returns middleware that only parses json
app.use(bodyParser.json());
//Returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

//importing cors
import cors from "cors";
//enable the express server to respond to preflight requests
app.use(
  cors({
    origin: "*",
  })
);

// import {forgot_user_handler}   from '../path_handlers/put_apis.js';
import { forgot_user_handler } from "./path_handlers/put_apis.js";

// importing post apis modules
import {
  login_user_handler,
  create_profile_path_handler,
  Fill_timesheet_data_handler,
} from "./path_handlers/post_apis.js";

//importing get apis modules from path handlers
import {
  get_profile_details_handler,
  getting_profile_data,
  get_fill_timesheets_data,
  get_specific_timesheets,
} from "./path_handlers/get_apis.js";

//importing validate_login_params,login_authentication_validate_forget_prams..from middlewares
import validate_login_params from "./middleware/login_validation.js";
import login_authentication from "./middleware/login_check.js";
import validate_forget_params from "./middleware/forget_validation.js";
import {
  register_param_count_checker,
  register_data_validator,
  checking_data_base,
} from "./middleware/register_validation.js";
import {
  fill_timesheet_params,
  filltimesheet_database_validation_for_dupilicate_data,
} from "./middleware/fill_timesheet_params.js";

/**
 * @path /login/:user_type checks for the type of user to logjn
 * @path "validate_login_params" is the middleware that contains the data that passed from the client(postman)
 * checks for the keys,data_type,not_empty_fields
 * @param login_user_handler checks for the user already existed in the database if present
 * logs succesfully otherwise gives error with status_message and code.
 */
app.post("/login/:user_type", validate_login_params, login_user_handler);

/**
 * @path /user_register checks for the type of user to register
 * @path "validate_params" is the middleware that contains the data that passed from the client(postman)
 * checks for the keys,data_type,not_empty_fields
 * @param "validate_another" is the middleware that contains the data that passed from the client(postman)
 * checks
 * @param create_profile_path_handler checks for the user already existed in the database if not
 * inserts the data into the database
 */
app.post(
  "/user_register",
  register_param_count_checker,
  register_data_validator,
  checking_data_base,
  create_profile_path_handler
);

/**
 * @path /get_profiles path to get profiles of employee
 * @param login_authentication is the middleware that contains the data that passed from client(postman)
 * verifies the jwt token
 * @param get_profile_details_handler is the path_handler that checks for the user existed in database
 * if present gets data from the user_registered database.
 */
app.get("/get_profiles", login_authentication, get_profile_details_handler);

/**
 * @path /get_specific_emp/:empid path to get profile of an employee
 * @param get_emp_profile_by_empid is the path handler checks for the employee based on id
 * if presents get_employee details otherwise throws an error
 */
app.get(
  "/get_specific_emp/:empid/:role",
  login_authentication,
  getting_profile_data
);

/**
 * @path /get_timesheets path to get timesheets of employee
 * @param  get_fill_timesheets is the path handler to get all the timesheets of employee
 * if presents it sends data otherwise it throws an with message
 */
app.get("/all_timesheets", login_authentication, get_fill_timesheets_data);

/**
 * @path /get_specific_timesheet path to get specific timesheet of an employee
 * @param get_specific_timesheet is the path handler to get specific timesheet
 * of an employee based on id
 * if presents sends data otherwise throws an error with message
 */
app.get(
  "/get_specific_timesheet/:empid",
  login_authentication,
  get_specific_timesheets
);

/**
 * @path /forgot path to update the password of an user
 * @param validate_forget_prams is the middleware to check the keys and params are valid
 * or not if valid it goes to next()
 * @param forgot_user_handler is the path handler that checks for the user existed or not
 * if existed checks validation and if validation is correct then reset the password
 * otherwise throws an error with message
 */
app.put("/forgot", validate_forget_params, forgot_user_handler);

/**
 * @path /fill_timesheet post path to fill the timesheet
 * @param login_authentication is the middleware that contains the data that passed from client(postman)
 * verifies the jwt token
 * @param Fill_timesheet_params is the middleware that checks for the params->data_type,keys,
 * not_empty_fields if all conditions satisfies goes to next()
 * @param Fill_timesheet_data_handler is the path_handler that inserts data into the database
 */
app.post(
  "/fill_timesheet",
  login_authentication,
  fill_timesheet_params,
  filltimesheet_database_validation_for_dupilicate_data,
  Fill_timesheet_data_handler
);

//app listening to the port
app.listen(3001, () => {
  console.log("http://localhost:3001");
});
