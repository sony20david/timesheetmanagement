// importing mongoose from mongoose
import mongoose from "mongoose";

/*
  creating a schema for Employee_login_data
*/

const LoginSchema = new mongoose.Schema({
  username: String,
  password: String,
  jwt_token: String,
  logon_time: Date,
});

//creating schema for createprofile
const CreateProfileSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  empid: {
    type: Number,
    required: true,
  },
  yearofjoining: {
    type: Number,
    required: true,
  },
  role: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  phoneno: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  security_question: {
    type: String,
    required: true,
  },
  gender: {
    type: String,
    required: true,
  },
  city: {
    type: String,
    required: true,
  },
  district: {
    type: String,
    required: true,
  },
  state: {
    type: String,
    required: true,
  },
  country: {
    type: String,
    required: true,
  },
  bloodgroup: {
    type: String,
    required: true,
  },
  position: {
    type: String,
    required: true,
  },
});

//creating schema for the filltimesheet
const FillTimesheetSchema = new mongoose.Schema({
  documentation_presentation: {
    hours: {
      type: Number,
    },

    description: {
      type: String,
    },
  },
  training_learning: {
    hours: {
      type: Number,
    },

    description: {
      type: String,
    },
  },
  proposals_technical: {
    hours: {
      type: Number,
    },

    description: {
      type: String,
    },
  },
  hiring_interviewing: {
    hours: {
      type: Number,
    },

    description: {
      type: String,
    },
  },
  delivery: {
    hours: {
      type: Number,
    },

    description: {
      type: String,
    },
  },
  appraisals: {
    hours: {
      type: Number,
    },

    description: {
      type: String,
    },
  },
  mentoring: {
    hours: {
      type: Number,
    },

    description: {
      type: String,
    },
  },
  meeting_customers: {
    hours: {
      type: Number,
    },

    description: {
      type: String,
    },
  },
  empid: {
    type: Number,
    required: true,
  },
  day: {
    type: Number,
    required: true,
  },
  month: {
    type: String,
    required: true,
  },
});
//creating schema for the timesheet
const TimesheetsSchema = new mongoose.Schema({
  date: Date,
  hours: Number,
  empid: String,
  dayspresent: Number,
  daysabsent: Number,
  empsalary: Number,
});

//exporting schemas to the model
export {
  LoginSchema,
  CreateProfileSchema,
  FillTimesheetSchema,
  TimesheetsSchema,
};
