import { response } from "express";
//importing mongoose from mongoose
import mongoose from "mongoose";
//import bcrypt
import bcrypt from "bcrypt";
// import register_data_schema from schema.js file
import {
  LoginSchema,
  CreateProfileSchema,
  FillTimesheetSchema,
  TimesheetsSchema,
} from "./schema.js";
// connecting the db
mongoose.connect(
  "mongodb+srv://main-test:Subbareddy@test.jxyoz.mongodb.net/myFirstDatabase?retryWrites=true&w=majority "
);

/* creating a model for the user_register_data for creating a list and enter objects in it */
const CREATE_EMPLOYEE_DETAILS_DATA = mongoose.model(
  "CREATE_EMPLOYEE_DETAILS_DATA",
  CreateProfileSchema
);
/* creating a model for the user_login_data for creating a list and enter objects in it */

const USER_LOGIN_DATA = mongoose.model("USER_LOGIN_DATA", LoginSchema);

/* creating a model for the hr_register_data for creating a list and enter objects in it */
const CREATE_HR_REGISTER_DATA = mongoose.model(
  "CREATE_HR_REGISTER_DATA",
  CreateProfileSchema
);

/* creating a model for the  for creating a fill time sheet and enter objects in it */
const FILL_TIME_SHEET_DATA = mongoose.model(
  "FILL_TIME_SHEET",
  FillTimesheetSchema
);

//asynchronous function uses the register_data
async function inserting_user_register_data_into_database(register_data) {
  // const Password = generatePassword()
  // const USER_LOGIN_data_1 = mongoose.model('USER_LOGIN_data_1', USER_LOGINPAGE_SCHEMA ); // schema
  const password = register_data.password;
  //declaring hashed password as bcrypt password
  const hashed_password = await bcrypt.hash(password, 10);
  //delcaring user_data as the CREATE_EMPLOYEE_DETAILS_DATA
  const user_data = new CREATE_EMPLOYEE_DETAILS_DATA({
    name: register_data.name,
    username: register_data.username,
    password: hashed_password,
    empid: register_data.empid,
    yearofjoining: register_data.yearofjoining,
    phoneno: register_data.phoneno,
    role: register_data.role,
    email: register_data.email,
    security_question: register_data.security_question,
    city: register_data.city,
    district: register_data.district,
    state: register_data.state,
    country: register_data.country,
    bloodgroup: register_data.bloodgroup,
    position: register_data.position,
    gender: register_data.gender,
  });
  // saving the data
  user_data
    .save()
    .then(() => console.log("data saved"))
    .catch((err) => {
      console.log(err);
    });
  //response.send("data added successfully")
}

//function using timesheet uses the timesheet_data and response to send the data into the db
/**
 *
 * @param  timesheet_data derives request body params
 * @param response as status obj
 */
function pushing_data_time_sheet(timesheet_data, response) {
  //declaring create_data as the FILL_TIME_SHEET_DATA
  const create_data = new FILL_TIME_SHEET_DATA({
    //declaring all the params objects in the create_data
    documentation_presentation: {
      hours: timesheet_data.documentation_presentation.hours,
      description: timesheet_data.documentation_presentation.description,
    },
    //declaring the training_learning obj contains hours and description
    training_learning: {
      hours: timesheet_data.training_learning.hours,
      description: timesheet_data.training_learning.description,
    },
    //declaring the proposals_technical obj contains hours and description
    proposals_technical: {
      hours: timesheet_data.proposals_technical.hours,
      description: timesheet_data.proposals_technical.description,
    }, //declaring the hiring_interviewing obj contains hours and description
    hiring_interviewing: {
      hours: timesheet_data.hiring_interviewing.hours,
      description: timesheet_data.hiring_interviewing.description,
    }, //declaring the delivery obj contains hours and description
    delivery: {
      hours: timesheet_data.delivery.hours,
      description: timesheet_data.delivery.description,
    }, //declaring the appraisals obj contains hours and description
    appraisals: {
      hours: timesheet_data.appraisals.hours,
      description: timesheet_data.appraisals.description,
    }, //declaring the mentoring obj contains hours and description
    mentoring: {
      hours: timesheet_data.mentoring.hours,
      description: timesheet_data.mentoring.description,
    }, //declaring the meeting_customers obj contains hours and description
    meeting_customers: {
      hours: timesheet_data.meeting_customers.hours,
      description: timesheet_data.meeting_customers.description,
    },
    //empid declares timesheet_data empid
    empid: timesheet_data.empid,
    day: timesheet_data.day,
    month: timesheet_data.month,
  });
  //saving the data
  create_data
    .save()
    //it sends response as obj with the status_code and status_msg
    .then(() =>
      response.send(
        JSON.stringify({
          status_code: 200,
          status_message: "success",
        })
      )
    )
    //catch block holds the error exceptions
    .catch((err) => {
      response.send(
        JSON.stringify({ status_code: 400, status_message: "db error occur" })
      );
    });
}

/**
 *
 * @param register_data derives request body params
 * asynchronous function uses the register data to push the data into the db
 */
async function inserting_hr_register_data_into_database(register_data) {
  //declaring password as register_data.password
  const password = register_data.password;
  //declaring hashed_password as using bcrypt hashing
  const hashed_password = await bcrypt.hash(password, 10);
  //declaring hr_data as CREATE_HR_REGISTER_DATA model
  const hr_data = new CREATE_HR_REGISTER_DATA({
    name: register_data.name,
    username: register_data.username,
    password: hashed_password,
    empid: register_data.empid,
    yearofjoining: register_data.yearofjoining,
    phoneno: register_data.phoneno,
    role: register_data.role,
    email: register_data.email,
    security_question: register_data.security_question,
    city: register_data.city,
    district: register_data.district,
    state: register_data.state,
    country: register_data.country,
    bloodgroup: register_data.bloodgroup,
    position: register_data.position,
    gender: register_data.gender,
  });
  // saving the data
  hr_data
    .save()
    .then(() => console.log("data saved"))
    .catch((err) => {
      console.log(err);
    });
  //response.send("data added successfully")
}

/**
 *
 * @param login_data from the request.body
 *  function uses the login data to push the data into the db
 */
function inserting_user_login_data_into_database(login_data) {
  //declaring date  as date model
  var date = new Date();
  login_data.date = date;
  //declaring user_login_data as USER_LOGIN_DATA model
  const user_login_data = new USER_LOGIN_DATA({
    username: login_data.username,
    password: login_data.password,
    jwt_token: login_data.jwt_token,
    logon_time: login_data.date,
  });
  //saving the data
  user_login_data
    .save()
    .then(() => console.log("data saved"))
    .catch((err) => {
      console.log(err);
    });
}

//exporting functions to utilizing in the require path
export {
  CREATE_EMPLOYEE_DETAILS_DATA,
  CREATE_HR_REGISTER_DATA,
  FILL_TIME_SHEET_DATA,
  inserting_user_register_data_into_database,
  inserting_hr_register_data_into_database,
  inserting_user_login_data_into_database,
  pushing_data_time_sheet,
};
