//importing data's from model.js
import {
  CREATE_EMPLOYEE_DETAILS_DATA,
  CREATE_HR_REGISTER_DATA,
} from "./model.js";
//importing function from model.js
import { inserting_user_login_data_into_database } from "./model.js";
//importing bcrypt
import bcrypt from "bcrypt";

/**
 * validate the user login data based on the user register database data
 * @param login_data that contains the user login data
 * @param response which is the response object that send the response to the client
 * @returns the different response like if username not found or password not correct or login success
 * based on the conditions with validating the user register database data
 */
function validate_user_login_data(login_data, response) {
  // fetching the user register data from the data base for login validatinn
  CREATE_EMPLOYEE_DETAILS_DATA.find(
    { username: login_data.username },
    async (err, data) => {
      // any error occur in the data fetching it prints in the console
      if (err) {
        // printing error in console
        console.log(err);
        //if there is no error in the data fetching it goes through the else block
      } else {
        // if there any data that matches with the username it goes through the if condition
        if (data.length !== 0) {
          login_data.emp_id = data[0].empid;
          // iterating over the data fetched from the database
          for (let item of data) {
            // validating the password
            const isPasswordMatch = await bcrypt.compare(
              login_data.password,
              item.password
            );
            if (isPasswordMatch) {
              // if password is valid it sends the response as 200 'success'
              // const x = is_login_before(login_data)
              // console.log(x)
              login_data.status_code = 200;
              inserting_user_login_data_into_database(login_data);
              response.send(JSON.stringify(login_data));
              break;
            }
            // if password is invalid it sends the response as password not match
            else {
              const obj = {
                status_code: 401,
                status_message: "invalid password",
              };
              response.send(JSON.stringify(obj));
              break;
            }
          }
        }
        // if fetching data count is empty it sends the response as user not found
        else {
          const obj = {
            status_code: 404,
            status_message: "username not found",
          };
          response.send(JSON.stringify(obj));
        }
      }
    }
  );
}

/**
 * validate the user login data based on the user register database data
 * @param login_data that contains the user login data
 * @param response which is the response object that send the response to the client
 * @returns the different response like if username not found or password not correct or login success
 * based on the conditions with validating the user register database data
 */
function validate_hr_login_data(login_data, response) {
  // fetching the user register data from the data base for login validatinn
  CREATE_HR_REGISTER_DATA.find(
    { username: login_data.username },
    async (err, data) => {
      // any error occur in the data fetching it prints in the console
      if (err) {
        // printing error in console
        console.log(err);
        //if there is no error in the data fetching it goes through the else block
      } else {
        // if there any data that matches with the username it goes through the if condition
        if (data.length !== 0) {
          login_data.emp_id = data[0].empid;
          // iterating over the data fetched from the database
          for (let item of data) {
            // validating the password
            const isPasswordMatch = await bcrypt.compare(
              login_data.password,
              item.password
            );
            if (isPasswordMatch) {
              // if password is valid it sends the response as 200 'success'
              // const x = is_login_before(login_data)
              // console.log(x)
              login_data.status_code = 200;
              inserting_user_login_data_into_database(login_data);
              response.send(JSON.stringify(login_data));
              break;
            }
            // if password is invalid it sends the response as password not match
            else {
              const obj = {
                status_code: 401,
                status_message: "invalid password",
              };
              response.send(JSON.stringify(obj));
              break;
            }
          }
        }
        // if fetching data count is empty it sends the response as user not found
        else {
          const obj = {
            status_code: 404,
            status_message: "username not found",
          };
          response.send(JSON.stringify(obj));
        }
      }
    }
  );
}

//assuming the admin login credentials as static
var admin_credentials = {
  username: "Mahesh",
  password: "Mahesh123@",
};

function validate_admin_login_data(login_data, response) {
  if (admin_credentials.username === login_data.username) {
    // iterating over the data fetched from the database
    // validating the password
    if (admin_credentials.password === login_data.password) {
      // if password is valid it sends the response as 200 'success'
      // const x = is_login_before(login_data)
      // console.log(x)
      login_data.status_code = 200;
      inserting_user_login_data_into_database(login_data);
      response.send(JSON.stringify(login_data));
    }
    // if password is invalid it sends the response as password not match
    else {
      const obj = {
        status_code: 401,
        status_message: "invalid password",
      };
      response.send(JSON.stringify(obj));
    }
  }
  // if fetching data count is empty it sends the response as user not found
  else {
    const obj = {
      status_code: 404,
      status_message: "username not found",
    };
    response.send(JSON.stringify(obj));
  }
}

const deleting_user_login_details_in_data_base = (email) => {
  USER_LOGIN_DATA.findOneAndRemove({ email: email }, (err) => {
    if (err) {
      console.log(err);
    }
  });
};

const deleting_admin_login_details_in_data_base = (id) => {
  ADMIN_LOGIN_DATA.findOneAndRemove({ id: id }, (err) => {
    if (err) {
      console.log(err);
    }
  });
};

/**
 * update_employee_password function used to update the password anytime...
 */
const update_employee_password = async (email, new_password) => {
  const hashed_password = await bcrypt.hash(new_password, 10);
  //try block catches the function without exceptions
  try {
    const result = await CREATE_EMPLOYEE_DETAILS_DATA.updateOne(
      { email: email },
      {
        $set: {
          password: hashed_password,
        },
      }
    );
    console.log(result);
  } catch (err) {
    //catch block catches the exception
    console.log(err);
  }
};
// this function used to help the update the hr password
const update_hr_password = async (email, new_password) => {
  //try block catches the function without exceptions
  try {
    const result = await CREATE_HR_REGISTER_DATA.updateOne(
      { email },
      {
        $set: {
          password: new_password,
        },
      }
    );
    console.log(result);
  } catch (err) {
    //catch block catches the exception
    console.log(err);
  }
};

/**
 * validate the user login data based on the user register database data
 * @param login_data that contains the user login data
 * @param response which is the response object that send the response to the client
 * @returns the different response like if username not found or password not correct or login success
 * based on the conditions with validating the user register database data
 */
function updating_employee_password(login_data, response, client_key) {
  // fetching the user login data
  //deleting_user_login_details_in_data_base(login_data.email)
  //fetching the user register data from the data base for login validatinn
  CREATE_EMPLOYEE_DETAILS_DATA.find(
    {
      email: login_data.email,
      security_question: login_data.security_question,
    },
    (err, data) => {
      // any error occur in the data fetching it prints in the console
      if (err) {
        // printing error in console
        console.log(err);
        //if there is no error in the data fetching it goes through the else block
      } else {
        // if there any data that matches with the username it goes through the if condition
        if (data.length !== 0) {
          // iterating over the data fetched from the database
          update_employee_password(data[0].email, login_data.new_password);
          response.send(
            JSON.stringify({
              status_code: 200,
              status_message: "password updated successfully",
            })
          );
        }
        // if fetching data count is empty it sends the response as user not found
        else {
          response.send(
            JSON.stringify({
              status_code: 404,
              status_message: "Please enter correct email or security question",
            })
          );
        }
      }
    }
  );
}

/**
 * validate the admin login data based on the admin register database data
 * @param login_data that contains the admin login data
 * @param response which is the response object that send the response to the client
 * @param client_key it contains the jwt_token that are passed from the request headers
 * @returns the different response like if username not found or password not correct or login success
 * based on the conditions with validating the admin register database data
 */
function updating_hr_password(login_data, response, client_key) {
  // fetching the user login data
  //fetching the user register data from the data base for login validatinn
  CREATE_HR_REGISTER_DATA.find({ email: login_data.email }, (err, data) => {
    // any error occur in the data fetching it prints in the console
    if (err) {
      // printing error in console
      console.log(err);
      //if there is no error in the data fetching it goes through the else block
    } else {
      // if there any data that matches with the username it goes through the if condition
      if (data.length !== 0) {
        console.log(data);
        // updating the password by calling the update function
        update_hr_password(data[0].email, login_data.new_password);
        response.send(
          JSON.stringify({
            status_code: 200,
            status_message: "password updated successfully",
          })
        );
      }
      // if fetching data count is empty it sends the response as user not found
      else {
        response.send(
          JSON.stringify({
            status_code: 404,
            status_message: "Please enter correct email or security question",
          })
        );
      }
    }
  });
}
//exporting functions
export {
  validate_user_login_data,
  validate_hr_login_data,
  validate_admin_login_data,
  updating_employee_password,
  updating_hr_password,
};
