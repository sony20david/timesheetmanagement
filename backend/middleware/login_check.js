//importing jwt from jsonwebtoken
import jwt from "jsonwebtoken";
//importing jwt_token from dotenv file
import dotenv from "dotenv";
import ACCESS_TOKEN_SECRET from "dotenv";
dotenv.config();
import mongoose from "mongoose";
// const USER_LOGIN_DATA=mongoose.model("USER_LOGIN_DATA")

/**
 *
 * @param req  request from the postman or client
 * @param res response to the client
 * @param next next to go for next middleware if successfull of authentication
 * login_authenticaiton function verifies the jwt_token if successfull goes to next()
 */
const login_authentication = (req, res, next) => {
  const { authorization } = req.headers;
  if (!authorization) {
    res.status(401).json({ error: "you must be logged in" });
  }
  const token = authorization.replace("Bearer ", "");

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) {
      return res.status(401).json({ error: "you must be logged inside" });
    }
    next();
  });
};

//exporting login_athentication middleware
export default login_authentication;
