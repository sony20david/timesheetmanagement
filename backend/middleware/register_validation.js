import { request, response } from "express";
//importing models
import {
  CREATE_EMPLOYEE_DETAILS_DATA,
  CREATE_HR_REGISTER_DATA,
} from "../utilities/model.js";

/**
 *
 * @param request request from the client(postman)
 * @param response sends response to the client
 * @param next Callback argument to the middleware function, called "next" by convention.
 * checks for the valid params =>goes to valid(register_data) function
 * if the return status code is 200 goes to next middleware
 * else displays error message
 */
function valid(body_params) {
  if (Object.keys(body_params).length !== 16) {
    //if required number of params are not there it will send 421 status code
    return {
      status_code: 406,
      status_message: "Parameters count is not matches with database fields",
    };
  }
  let body_keys_list = [];
  for (let item in body_params) {
    body_keys_list.push(item);
  }
  //delaring a list of keys to validate the keys
  let key_list = [
    "name",
    "username",
    "password",
    "email",
    "phoneno",
    "security_question",
    "position",
    "bloodgroup",
    "city",
    "district",
    "state",
    "country",
    "gender",
    "yearofjoining",
    "empid",
    "role",
  ];
  for (let key of key_list) {
    if (body_keys_list.includes(key)) {
      continue;
    } else {
      let msg = key + " is Missing!";
      return { status_code: 406, status_message: msg };
    }
  }
  return { status_code: 200 };
}

/**
 *
 * @param request  request from the client(postman)
 * @param response response to the client based on the request
 * @param next if register_param_count_checker satisfies all the conditions then goes to
 * next()
 */
function register_param_count_checker(request, response, next) {
  const register_data = request.body;
  const validate_params = valid(register_data);
  if (validate_params.status_code !== 200) {
    console.log(validate_params.status_code);
    response.send(JSON.stringify(validate_params));
  } else {
    next();
  }
}

/**
 *
 * @param request request from the client(postman)
 * @param response sends response to the client
 * @param next Callback argument to the middleware function
 */
function timesheet_param_count_checker(request, response, next) {
  param_count_checker(request, response, next, [
    "documentation_presentation",
    "training_learning",
    "empid",
    "proposals_technical",
    "hiring_interviewing",
    "delivery",
    "appraisals",
    "mentoring",
  ]);
}

/**
 *
 * @param  req request from the client(postman)
 * @param  res response from the server to client
 * @param  next Callback argument to the middleware function, called "next" by convention.
 * function for checking unique params=>email,empid,phoneno is already existed or not
 * if existed send status message email,phoneno,empid must be unique
 * if not goes to path_handler
 */
function checking_data_base(req, res, next) {
  const register_data = req.body;
  if (register_data.position === "employee") {
    const { email, empid, phoneno, username } = register_data;
    // checking whether the employee id is unique or not
    CREATE_EMPLOYEE_DETAILS_DATA.find(
      {
        $or: [
          { email: email },
          { empid: empid },
          { phoneno: phoneno },
          { username: username },
        ],
      },
      (error, data) => {
        if (data.length !== 0) {
          console.log("hello ram");
          if (data[0].email === email) {
            res.send(
              JSON.stringify({
                status_message: "email alredy taken",
                status_code: 409,
              })
            );
            // checking whether the email is unique or not
          } else if (parseInt(data[0].phoneno) === phoneno) {
            res.send(
              JSON.stringify({
                status_message: "phone no already taken",
                status_code: 409,
              })
            );
          } else if (data[0].username === username) {
            res.send(
              JSON.stringify({
                status_message: "username already exist",
                status_code: 409,
              })
            );
          } else if (data[0].empid === empid) {
            res.send(
              JSON.stringify({
                status_message: "empid alredy exist",
                status_code: 409,
              })
            );
          }
        } else {
          // response.send("success")
          next();
        }
      }
    );
  } else {
    const { email, empid, phoneno, username } = register_data;
    // checking whether the employee id is unique or not
    CREATE_HR_REGISTER_DATA.find(
      {
        $or: [
          { email: email },
          { empid: empid },
          { phoneno: phoneno },
          { username: username },
        ],
      },
      (error, data) => {
        if (data.length !== 0) {
          if (data[0].email === email) {
            res.send(JSON.stringify("email alredy taken"));
            // checking whether the email is unique or not
          } else if (parseInt(data[0].phoneno) === phoneno) {
            res.send(JSON.stringify("phone no already taken"));
          } else if (data[0].username === username) {
            res.send(JSON.stringify("username already exist"));
          } else if (data[0].empid === empid) {
            res.send(JSON.stringify("empid alredy exist"));
          }
        } else {
          // response.send("success")
          next();
        }
      }
    );
  }
}

/**
 *
 * @param request request from the client
 * @param response sends response to the client
 * @param next Callback argument to the middleware function, called "next" by convention.
 * checking params one by one with our required specifications
 * if not returns error messages with statuscode
 * if all params satisfies the condition goes to next middleware
 */
const register_data_validator = (request, response, next) => {
  //destructuring items from the request.body
  const {
    name,
    empid,
    yearofjoining,
    password,
    email,
    phoneno,
    role,
    username,
    security_question,
    bloodgroup,
    gender,
    position,
    city,
    district,
    state,
    country,
  } = request.body;
  const name_validation_result = _name_helper(name);
  const email_validation_email = _email_helper(email);
  const empid_validation_result = _empid_helper(empid);
  const year_of_joining_validation_result =
    _year_of_joining_helper(yearofjoining);
  const phone_no_validation_result = _phoneNo_helper(phoneno);
  const role_validation_result = _role_helper(role);
  const username_validation_result = _name_helper(username);
  const security_question_validation_result =
    _security_question_helper(security_question);
  const bloodgroup_validation = _bloodgroup_helper(bloodgroup);
  const gender_validation = _gender_helper(gender);
  const city_validation = _city_helper(city);
  const district_validation = _district_helper(district);
  const state_validation = _state_helper(state);
  const country_validation = _country_helper(country);

  const position_validation = _position_helper(position);
  //console.log(`error obj ${error_object}`);
  const password_validation_result = _password_helper(password);
  if (name_validation_result.status_code !== 200) {
    response.send(JSON.stringify(name_validation_result));
  } else if (email_validation_email.status_code !== 200) {
    response.send(JSON.stringify(email_validation_email));
  } else if (empid_validation_result.status_code !== 200) {
    response.send(JSON.stringify(empid_validation_result));
  } else if (year_of_joining_validation_result.status_code !== 200) {
    response.send(JSON.stringify(year_of_joining_validation_result));
  } else if (phone_no_validation_result.status_code !== 200) {
    response.send(JSON.stringify(phone_no_validation_result));
  } else if (role_validation_result.status_code !== 200) {
    response.send(JSON.stringify(role_validation_result));
  } else if (position_validation.status_code !== 200) {
    response.send(JSON.stringify(position_validation));
  } else if (city_validation.status_code !== 200) {
    response.send(JSON.stringify(city_validation));
  } else if (gender_validation.status_code !== 200) {
    response.send(JSON.stringify(gender_validation));
  } else if (district_validation.status_code !== 200) {
    response.send(JSON.stringify(district_validation));
  } else if (state_validation.status_code !== 200) {
    response.send(JSON.stringify(state_validation));
  } else if (country_validation.status_code !== 200) {
    response.send(JSON.stringify(country_validation));
  } else if (bloodgroup_validation.status_code !== 200) {
    response.send(JSON.stringify(bloodgroup_validation));
  } else if (username_validation_result.status_code !== 200) {
    response.send(JSON.stringify(username_validation_result));
  } else if (security_question_validation_result.status_code !== 200) {
    response.send(JSON.stringify(security_question_validation_result));
  } else if (password_validation_result.status_code !== 200) {
    response.send(JSON.stringify(password_validation_result));
  } else {
    next();
  }
};

/**
 *
 * @param  password
 * @returns if password credentials satisfies returns status_code 200
 * checks for the datatype,length if not satisfies returns error status code
 * with message
 */
const _password_helper = (password) => {
  const returnObject = {};
  // is it a string
  if (typeof password != "string") {
    returnObject.password = "password should be of string type";
    return returnObject;
  }
  if (password.length < 10) {
    returnObject.password =
      "password length should be at least 10 characters long";
    return returnObject;
  }
  if (password.length > 20) {
    returnObject.password = "password length should be a max of 20";
    return returnObject;
  }
  if (
    !password.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{10,20}$/)
  ) {
    returnObject.password = "password should be of the correct format";
    return returnObject;
  }

  return { status_code: 200 };
};

/**
 *
 * @param  name
 * @returns if name credentials satisfies returns status_code 200
 * checks for the datatype,length if not satisfies returns error status code
 * with message
 */
const _name_helper = (name) => {
  const returnObject = {};

  if (typeof name != "string") {
    returnObject.status_message = "name should be of string type";
    returnObject.status_code = 400;
    return returnObject;
  }

  if (name.length < 4) {
    returnObject.status_message =
      "name length should be at least 4 characters long";
    returnObject.status_code = 400;
    return returnObject;
  }

  if (name.length > 25) {
    returnObject.status_message = "name length shouldn't be 25 characters long";
    returnObject.status_code = 400;
    return returnObject;
  }

  if (!name.match(/^[a-zA-Z ]+$/)) {
    returnObject.status_message = "name should be of the correct format";
    returnObject.status_code = 400;
    return returnObject;
  }

  return { status_code: 200 };
};

/**
 *
 * @param email
 * @returns if email credentials satisfies returns status_code 200
 * checks for the datatype,length if not satisfies returns error status code
 * with message
 */
const _email_helper = (email) => {
  const returnObject = {};
  // is it a string
  if (typeof email != "string") {
    returnObject.status_message = "email should be of string type";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (email.length < 6) {
    returnObject.status_message =
      "email length should be at least 6 characters long";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (email.length > 50) {
    returnObject.status_message = "email length should be a max of 50";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (!email.endsWith("@innominds.com")) {
    returnObject.status_message = "email should be of the correct format";
    returnObject.status_code = 400;
    return returnObject;
  }

  return { status_code: 200 };
};

/**
 *
 * @param  empid
 * @returns if empid credentials satisfies returns status_code 200
 * checks for the datatype,length if not satisfies returns error status code
 * with message
 */
const _empid_helper = (empid) => {
  const returnObject = {};

  if (typeof empid != "number") {
    returnObject.status_message = "empid should be of Number type";
    returnObject.status_code = 400;
    return returnObject;
  }

  if (empid.toString().length < 5) {
    returnObject.status_message =
      "empid length should be at least 6 characters long";
    returnObject.status_code = 400;
    return returnObject;
  }

  if (empid.length > 15) {
    returnObject.status_message =
      "empid length should not be greater than 15 characters long";
    returnObject.status_code = 400;
    return { status_code: 200 };
  }
  return { status_code: 200 };
};

/**
 *
 * @param  yearofjoining
 * @returns if yearofjoining credentials satisfies returns status_code 200
 * checks for the datatype,length if not satisfies returns error status code
 * with message
 */
const _year_of_joining_helper = (dateofjoining) => {
  const returnObject = {};

  if (typeof dateofjoining != "number") {
    returnObject.status_message = "year of joining should be of number type";
    returnObject.status_code = 400;
    return returnObject;
  }

  if (dateofjoining.toString().length < 4) {
    returnObject.status_message =
      "year ofjoining length should be at least 6 characters long";
    returnObject.status_code = 400;
    return returnObject;
  }

  if (dateofjoining.length > 21) {
    returnObject.status_message =
      "year ofjoining length should have 8 characters long";
    returnObject.status_code = 400;
    return returnObject;
  }
  return { status_code: 200 };
};

/**
 *
 * @param  phoneno
 * @returns if phoneno credentials satisfies returns status_code 200
 * checks for the datatype,length if not satisfies returns error status code
 * with message
 */
const _phoneNo_helper = (phoneno) => {
  const returnObject = {};

  if (typeof phoneno != "number") {
    returnObject.status_message = "phoneNo should be of Number type";
    returnObject.status_code = 400;
    return returnObject;
  }

  if (phoneno.toString().length < 10) {
    returnObject.status_message =
      "phone number length should be at least 10 characters long";
    returnObject.status_code = 400;
    return returnObject;
  }

  if (phoneno.length > 10) {
    returnObject.status_message = "phone number length should be a max of 10";
    returnObject.status_code = 400;
    return returnObject;
  }

  return { status_code: 200 };
};

/**
 *
 * @param position
 * @returns if position credentials satisfies returns status_code 200
 * checks for the datatype,length if not satisfies returns error status code
 * with message
 */
const _position_helper = (position) => {
  const position_list = ["hr", "employee"];
  const returnObject = {};
  if (typeof position != "string") {
    returnObject.status_message = "position should be of string type";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (!position_list.includes(position)) {
    // console.log(!(role_list.includes(role)));
    returnObject.status_message = "the position is must either employee or hr";
    returnObject.status_code = 400;
    return returnObject;
  }

  return { status_code: 200 };
};

/**
 *
 * @param  gender
 * @returns if gender credentials satisfies returns status_code 200
 * checks for the datatype,length if not satisfies returns error status code
 * with message
 */
const _gender_helper = (gender) => {
  const gender_list = ["male", "female", "other"];
  const returnObject = {};
  if (typeof gender != "string") {
    returnObject.status_message = "gender should be of string type";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (!gender_list.includes(gender)) {
    // console.log(!(role_list.includes(role)));
    returnObject.status_message =
      "the gender is must either male ,female or other";
    returnObject.status_code = 400;
    return returnObject;
  }

  return { status_code: 200 };
};

/**
 *
 * @param role
 * @returns if role credentials satisfies returns status_code 200
 * checks for the datatype,length if not satisfies returns error status code
 * with message
 */
const _role_helper = (role) => {
  const returnObject = {};

  if (typeof role != "string") {
    returnObject.status_message = "role should be of string type";
    returnObject.status_code = 400;
    return returnObject;
  }

  return { status_code: 200 };
};

/**
 *
 * @param  security_question
 * @returns if securityquestion credentials satisfies returns status_code 200
 * checks for the datatype,length if not satisfies returns error status code
 * with message
 */
const _security_question_helper = (security_question) => {
  const returnObject = {};

  if (typeof security_question != "string") {
    returnObject.status_message = "security_question should be of string type";
    returnObject.status_code = 400;
    return returnObject;
  }

  return { status_code: 200 };
};

/**
 *
 * @param city
 * @returns if city credentials satisfies returns status_code 200
 * checks for the datatype,length if not satisfies returns error status code
 * with message
 */
const _city_helper = (city) => {
  const returnObject = {};

  if (typeof city != "string") {
    returnObject.status_message = "address should be of string type";
    returnObject.status_code = 400;
    return returnObject;
  }

  return { status_code: 200 };
};

/**
 *
 * @param district
 * @returns if district credentials satisfies returns status_code 200
 * checks for the datatype,length if not satisfies returns error status code
 * with message
 */
const _district_helper = (district) => {
  const returnObject = {};

  if (typeof district != "string") {
    returnObject.status_message = "district should be of string type";
    returnObject.status_code = 400;
    return returnObject;
  }

  return { status_code: 200 };
};

/**
 *
 * @param state
 * @returns if state credentials satisfies returns status_code 200
 * checks for the datatype,length if not satisfies returns error status code
 * with message
 */
const _state_helper = (state) => {
  const returnObject = {};

  if (typeof state != "string") {
    returnObject.status_message = "address should be of string type";
    returnObject.status_code = 400;
    return returnObject;
  }

  return { status_code: 200 };
};

/**
 *
 * @param  country
 * @returns if country credentials satisfies returns status_code 200
 * checks for the datatype,length if not satisfies returns error status code
 * with message
 */
const _country_helper = (country) => {
  const returnObject = {};

  if (typeof country != "string") {
    returnObject.status_message = "country should be of string type";
    returnObject.status_code = 400;
    return returnObject;
  }
  return { status_code: 200 };
};

/**
 *
 * @param bloodgroup
 * @returns if bloodgroup credentials satisfies returns status_code 200
 * checks for the datatype,length if not satisfies returns error status code
 * with message
 */
const _bloodgroup_helper = (bloodgroup) => {
  const returnObject = {};

  if (typeof bloodgroup != "string") {
    returnObject.status_message = "bloodgroup should be of string type";
    returnObject.status_code = 400;
    return returnObject;
  }

  return { status_code: 200 };
};

//exporting functions to the server to utilize in the register api
export {
  register_param_count_checker,
  timesheet_param_count_checker,
  register_data_validator,
  checking_data_base,
};
