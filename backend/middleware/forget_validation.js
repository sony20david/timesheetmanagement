/**
 * Helper function to validate password string
 * checks if password is string, else returns "password should be of string type"
 * checks if password is too short, else returns "password should be at least 10 characters long and max of 20"
 * checks if password is of the correct format, else returns "password should have at least 1 special character and at least 1 digit"
 * @param password
 * @returns if fucntion satisfies all the conditions then staus_code "200"
 * otherwise returns error message ,status_code as an object
 */
const _password_helper = (password) => {
  const returnObject = {};
  // is it a string
  if (typeof password != "string") {
    returnObject.status_message = "password should be of string type";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (password.length < 10) {
    returnObject.status_message =
      "password length should be at least 10 characters long";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (password.length > 20) {
    returnObject.status_message = "password length should be a max of 20";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (
    !password.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{10,20}$/)
  ) {
    returnObject.status_message =
      "password should contains atleast one special char and number and captial letter";
    returnObject.status_code = 400;
    return returnObject;
  }
  return { status_code: 200 };
};

/**
 * Helper function to validate email string
 * checks if email is string, else returns "email should be of string type"
 * checks if email is too short, else returns "email should be at least 6 characters long and max of 50"
 * checks if email is of the correct format, else returns "email should be of the correct format" 406
 * @param {} email
 * @returns if all conditions satisfies then status_code 200
 * otherwise returns staus_code=>400,error message as an object
 */
const _email_helper = (email) => {
  const returnObject = {};
  // is it a string
  if (typeof email != "string") {
    returnObject.status_message = "email should be of string type";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (email.length < 6) {
    returnObject.status_message =
      "email length should be at least 6 characters long";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (email.length > 50) {
    returnObject.status_message = "email length should be a max of 50";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (!email.endsWith("@innominds.com")) {
    returnObject.status_message = "email should be of the correct format";
    returnObject.status_code = 400;
    return returnObject;
  }

  return { status_code: 200 };
};

/**
 * Helper function to validate keys_of_forget_Data
 * @param {} forget_data
 * @returns if conditions of name satisfies returns param
 * otherwise returns errors
 */
function forget_data_keys_validation(forget_data) {
  let param_list = ["email", "security_question", "new_password", "position"];
  let param = "";
  for (let item in forget_data) {
    if (!param_list.includes(item)) {
      param = item;
    }
  }
  return param;
}

/**
 * Helper function to validate empty_forget_fields
 * @param {} forget_data
 * @returns if conditions of name satisfies returns true
 * otherwise returns false
 */
function empty_forget_fields(forget_data) {
  const { email, new_password, security_question, positon } = forget_data;
  if (
    email === "" ||
    new_password === "" ||
    security_question === "" ||
    positon === ""
  ) {
    return true;
  }
  return false;
}

/**
 * Helper function to validate security_question string
 * checks if security_question is string, else returns "security_question should be of string"
 * @param {} security_question
 * @returns if all conditions satisfies then status_code=>200
 * otherwise return status_code,message as object
 */
const _security_question_helper = (security_question) => {
  const returnObject = {};
  if (typeof security_question != "string") {
    returnObject.security_question =
      "security_question should be of string type";
    returnObject.status_code = 400;
    return returnObject;
  }
  return { status_code: 200 };
};

/**
 * Helper function to validate forget data
 * @param {} request request from the client(postman)
 * @param {} response response to the client based on the request
 * @returns if conditions of name satisfies then go to next()
 * otherwise returns errors with status_code as objects
 */
const validate_forget_params = (request, response, next) => {
  const forget_data = request.body;
  const keys_validations = forget_data_keys_validation(forget_data);
  const empty_fields = empty_forget_fields(forget_data);
  const password_validate_result = _password_helper(forget_data.new_password);
  const email_validate_result = _email_helper(forget_data.email);
  const security_validate_result = _security_question_helper(
    forget_data.security_question
  );
  if (keys_validations) {
    response.send(
      JSON.stringify({
        status_code: 400,
        status_message: `${keys_validations} is not a valid key`,
      })
    );
  } else if (empty_fields) {
    response.send(
      JSON.stringify({
        status_code: 400,
        status_message: "please fill all fields",
      })
    );
  } else if (password_validate_result.status_code !== 200) {
    response.send(JSON.stringify(password_validate_result));
  } else if (email_validate_result.status_code !== 200) {
    response.send(JSON.stringify(email_validate_result));
  } else if (security_validate_result.status_code !== 200) {
    response.send(JSON.stringify(security_validate_result));
  } else {
    next();
  }
};

//exporting validate_forget_para
export default validate_forget_params;
