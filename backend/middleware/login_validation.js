/**
 * Helper function to validate name string
 * @param  password
 * @returns if password credentials satisfies returns status_code 200
 * checks for the datatype,length if not satisfies returns error status code
 * with message as objects
 */
const _password_helper = (password) => {
  const returnObject = {};
  // is it a string
  if (typeof password != "string") {
    returnObject.status_message = "password should be of string type";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (password.length < 10) {
    returnObject.status_message =
      "password length should be at least 10 characters long";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (password.length > 20) {
    returnObject.status_message = "password length should be a max of 20";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (
    !password.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{10,20}$/)
  ) {
    returnObject.status_message =
      "password should contains atleast one special char and number and captial letter";
    returnObject.status_code = 400;
    return returnObject;
  }
  return { status_code: 200 };
};

/**
 * Helper function to validate name string
 * @param  login_data
 * @returns checks for empty fields if not satisfies returns false
 * otherwise it returns true
 */
function empty_login_fields(login_data) {
  const { username, password } = login_data;
  if (!username || !password) {
    return true;
  }
  return false;
}

/**
 * Helper function to validate name string
 * checks if name is string, else returns "name should be of string type"
 * checks if name is too short, else returns "name should be at least 4 characters long and max of 25"
 * checks if name is of the correct format, else returns "name should not have at least 1 special character and at least 1 digit"
 * @param {} name
 * @returns if conditions of name satisfies returns status code "200"
 * otherwise returns errors with status_code as objects
 */
const _username_helper = (name) => {
  const returnObject = {};
  if (typeof name != "string") {
    returnObject.status_message = "name should be of string type";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (name.length < 4) {
    returnObject.status_message =
      "name length should be at least 4 characters long";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (name.length > 25) {
    returnObject.status_message = "name length shouldn't be 25 characters long";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (!name.match(/^[a-zA-Z ]+$/)) {
    returnObject.status_message = "name should be of the correct format";
    returnObject.status_code = 400;
    return returnObject;
  }
  return { status_code: 200 };
};

/**
 * Helper function to validate name string
 * @param {} login_data
 * @returns if conditions of name satisfies returns param
 * otherwise returns error
 */
function login_data_keys_validation(login_data) {
  let param_list = ["username", "password"];
  let param = "";
  for (let item in login_data) {
    if (!param_list.includes(item)) {
      param = item;
    }
  }
  return param;
}

/**
 * This is a helper middleware function to help with validation of login
 * parameters.
 * @param req   HTTP req object as recieved by Express backend
 * @param res   HTTP response object to be populated by Express backend
 * @param next  next() is a function to be called if this middlware validation is success
 * validate_login_params is a function for validating all the possibilities
 * if all conditions satisfies it goes to next() if not
 * @returns status_code with message
 */
const validate_login_params = (request, response, next) => {
  //initializing login_data as the request.body
  const login_data = request.body;
  //initializing keys_validation as login_data_keys_validation
  const keys_validations = login_data_keys_validation(login_data);
  //initializing empty_fields as empty_login_fields
  const empty_fields = empty_login_fields(login_data);
  //initializing password_validate as _password_helper
  const password_validate = _password_helper(login_data.password);
  //initializing validate_username as _username_helper
  const validate_username = _username_helper(login_data.username);
  if (keys_validations) {
    response.send(
      JSON.stringify({
        status_code: 422,
        status_message: `${keys_validations} is not a valid key`,
      })
    );
  } else if (empty_fields) {
    response.send(
      JSON.stringify({
        status_code: 401,
        status_message: "username and password not be empty",
      })
    );
  } else if (password_validate.status_code !== 200) {
    response.send(JSON.stringify(password_validate));
  } else if (validate_username.status_code !== 200) {
    response.send(JSON.stringify(validate_username));
  } else {
    next();
  }
};

//exporting validate_login_params to the server to utilizing in the login api
export default validate_login_params;
