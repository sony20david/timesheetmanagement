//importing fill_time_sheet_data from model
import { FILL_TIME_SHEET_DATA } from "../utilities/model";

/**
 *
 * @param timesheet_data is data to check keys if any key is missing
 * @returns if key is not present then sends param otherwise throws an error
 */
function timesheet_keys_validation(timesheet_data) {
  let param_list = [
    "documentation_presentation",
    "day",
    "meeting_customers",
    "training_learning",
    "proposals_technical",
    "hiring_interviewing",
    "delivery",
    "appraisals",
    "mentoring",
    "empid",
    "month",
  ];
  //initializing param as empty
  let param = "";
  //using for loop in timesheet_data
  for (let item in timesheet_data) {
    if (!param_list.includes(item)) {
      param = item;
    }
  }
  return param;
}

/**
 *
 * @param timesheet_data is data to check is there any empty field
 * @returns it will check for atleast one should not empty and hours should not be "0"
 * if these conditions satisfies then goes to next() middleware
 * otherwise returns status_code 400 with error message
 */
function empty_timesheet_fields(timesheet_data) {
  //destructuring the params from timesheet_data...
  const {
    documentation_presentation,
    training_learning,
    proposals_technical,
    hiring_interviewing,
    delivery,
    appraisals,
    mentoring,
    meeting_customers,
  } = timesheet_data;
  //if this condition satisfies it returns status obj
  if (
    documentation_presentation.hours !== "" &&
    documentation_presentation.description === "" &&
    documentation_presentation.hours !== 0
  ) {
    return {
      status_code: 400,
      status_message: "please fill description for documentation_presentation",
    };
  }
  //if this condition satisfies it returns status obj
  else if (
    training_learning.hours !== "" &&
    training_learning.description === "" &&
    training_learning.hours !== 0
  ) {
    return {
      status_code: 400,
      status_message: "please fill description for training_learning",
    };
  } //if this condition satisfies it returns status obj
  else if (
    proposals_technical.hours !== "" &&
    proposals_technical.description === "" &&
    proposals_technical.hours !== 0
  ) {
    return {
      status_code: 400,
      status_message: "please fill description for proposals_technical",
    };
  } //if this condition satisfies it returns status obj
  else if (
    hiring_interviewing.hours !== "" &&
    hiring_interviewing.description === "" &&
    hiring_interviewing.hours !== 0
  ) {
    return {
      status_code: 400,
      status_message: "please fill description for hiring_interviewing",
    };
  } //if this condition satisfies it returns status obj
  else if (
    delivery.hours !== "" &&
    delivery.description === "" &&
    delivery.hours !== 0
  ) {
    return {
      status_code: 400,
      status_message: "please fill description for delivery",
    };
  } //if this condition satisfies it returns status obj
  else if (
    appraisals.hours !== "" &&
    appraisals.description === "" &&
    appraisals.hours !== 0
  ) {
    return {
      status_code: 400,
      status_message: "please fill description for appraisals",
    };
  } //if this condition satisfies it returns status obj
  else if (
    mentoring.hours !== "" &&
    mentoring.description === "" &&
    mentoring.hours !== 0
  ) {
    return {
      status_code: 400,
      status_message: "please fill description for mentoring",
    };
  } //if this condition satisfies it returns status obj
  else if (
    meeting_customers.hours !== "" &&
    meeting_customers.description === "" &&
    meeting_customers.hours !== 0
  ) {
    return {
      status_code: 400,
      status_message: "please fill description for meeting_customers",
    };
  } //if this condition notsatisfies it returns status obj
  else {
    return {
      status_code: 200,
    };
  }
}

/**
 *
 * @param req request from the client(postman)
 * @param res response to the client based on the requirement
 * @param next if all conditions satisfies it goes to next
 * fill_timesheet_params is function to check keys,empty fields if these conditions
 * satisfies it goes to next() otherwise sends status_code with error message
 */
const fill_timesheet_params = (req, res, next) => {
  let timesheet_data = req.body;
  const keys_validation = timesheet_keys_validation(timesheet_data);
  const empty_fields = empty_timesheet_fields(timesheet_data);
  if (keys_validation) {
    res.send(
      JSON.stringify({
        status_code: 400,
        status_message: `${keys_validation} is not a valid key`,
      })
    );
  } else if (empty_fields.status_code === 400) {
    res.send(JSON.stringify(empty_fields));
  } else {
    next();
  }
};

/**
 *
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * @param response is the callback parameter that sends the data based on the request data
 * function is to check wether the employee already filled the time sheet or not with day and month
 * if already the employee filled the timesheet it shows an error status_code:400
 * with error message already filled
 * @param next if not filled the timsheet it will go to next()
 */
const filltimesheet_database_validation_for_dupilicate_data = (
  request,
  response,
  next
) => {
  const { empid, day, month } = request.body;
  FILL_TIME_SHEET_DATA.find({ empid, day, month }, (error, data) => {
    if (error) {
      console.log(error);
    } else {
      if (data.length === 0) {
        next();
      } else {
        response.send(
          JSON.stringify({
            status_code: 400,
            status_message: `You already filled timesheet for ${day} ${month}`,
          })
        );
      }
    }
  });
};

//exporting fill_timesheet_params
export {
  fill_timesheet_params,
  filltimesheet_database_validation_for_dupilicate_data,
};
