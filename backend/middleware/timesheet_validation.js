/**
 * @param  body_params checks the length of params
 * function valid_timesheet checks for the length,keys
 * if all conditoins satisfies then
 * @returns status code "200" if not retuns "406" with status_message
 */
function valid_timesheet(body_params) {
  if (Object.keys(body_params).length !== 9) {
    return {
      status_code: 406,
      status_message: "Parameters count is not matches with database fields",
    };
  }
  //initializing the body_keys_list as empty list
  let body_keys_list = [];
  //using for loop in body_params
  for (let item in body_params) {
    body_keys_list.push(item);
  }
  //initializing the keys_list as our required body_params
  let key_list = [
    "documentation_presentation",
    "training_learning",
    "empid",
    "proposals_technical",
    "hiring_interviewing",
    "delivery",
    "appraisals",
    "mentoring",
  ];
  // using for loop in key_list
  for (let key of key_list) {
    if (body_keys_list.includes(key)) {
      continue;
    } else {
      let msg = key + " is Missing!";
      return { status_code: 406, status_message: msg };
    }
  }
  return { status_code: 200 };
}

/**
 * @param request request from the client(postman)
 * @param response response to the client based on the request
 * @param next goes to next() if all conditions satisfies
 * validate_timesheet_data is the function for validating=>length and key
 * if satisfies all the condition it will go to the next middleware
 */
function validate_timesheet_data(request, response, next) {
  //initializing timesheet_data as request.body
  const timesheet_data = request.body;
  //here declaring validate_params as above validate_params function
  const validate_params = validate_params(timesheet_data);
  //if this condition satisfies it sends validate_params
  if (validate_params.status_code !== 200) {
    response.send(JSON.stringify(validate_params));
  }
  // if not it continues to next
  else {
    next();
  }
}

//exporting validate_timesheet_data to the server to utilize in the timesheet api.
export default validate_timesheet_data;
