import ActionTypes from "../constants/actionTypes";

const initialState = {
  loginData: {},
  profileData: {},
  timesheetData: [],
};
export const employeeLoginReducer = (
  state = initialState.loginData,
  { type, payload }
) => {
  switch (type) {
    case ActionTypes.SET_LOGIN_DATA:
      return { ...state, loginData: payload };

    default:
      return state;
  }
};

export const employeeProfileReducer = (
  state = initialState.profileData,
  { type, payload }
) => {
  switch (type) {
    case ActionTypes.SET_PROFILE_DATA:
      return { ...state, profileData: payload };

    default:
      return state;
  }
};

export const employeeTimesheetReducer = (
  state = initialState.timesheetData,
  { type, payload }
) => {
  switch (type) {
    case ActionTypes.SET_TIMESHEET_DATA:
      return { ...state, timesheetData: payload };

    default:
      return state;
  }
};

export const setInitialStateReducer = (state = initialState, { type }) => {
  switch (type) {
    case ActionTypes.SET_INITIAL_STATE_ON_LOGOUT:
      localStorage.removeItem("persist:Storage");
      return {};

    default:
      return state;
  }
};
