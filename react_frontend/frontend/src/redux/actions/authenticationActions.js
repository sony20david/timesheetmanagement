import actionTypes from "../constants/actionTypes";

export const setLoginData = (data) => {
  return {
    type: actionTypes.SET_LOGIN_DATA,
    payload: data,
  };
};

export const setProfileData = (profile_data) => {
  return {
    type: actionTypes.SET_PROFILE_DATA,
    payload: profile_data,
  };
};

export const setTimesheetData = (timesheet_data) => {
  return {
    type: actionTypes.SET_TIMESHEET_DATA,
    payload: timesheet_data,
  };
};

export const setInitialData = () => {
  return {
    type: actionTypes.SET_INITIAL_STATE_ON_LOGOUT,
  };
};
