//imported external dependencies
import React from "react";

//wrote the react context for gathering all the components
const TimeSheetContext = React.createContext({
  user_jwt_token: "",
  user_jwt_token_updater: () => {},
  admin_jwt_token: "",
  admin_jwt_token_updater: () => {},
  hr_jwt_token: "",
  hr_jwt_token_updater: () => {},
});

//exported the context for further actions
export default TimeSheetContext;
