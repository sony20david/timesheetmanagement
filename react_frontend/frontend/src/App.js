// import {BrowserRouter as Router,Switch,Route} from "react-router-dom"

import Login from "./components/Login/login";
import Hr from "./components/HrLogin/hr";
import Admin from "./components/AdminLogin/admin";
import Employee from "./components/EmployeeLogin/employee";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import Home from "./components/Home/home";
import AllEmployeesView from "./components/AllEmployeesView/allemployeesview";

import EmployeHome from "./components/EmployeHome/employehome";
import Timesheet from "./components/TimeSheet/timesheet";
import Filltimesheet from "./components/FilltimeSheet/filltimesheet";
import NotFound from "./components/NotFound/index";
import TimeSheetContext from "./Context/TmeSheetContext";
import EmployeeFullDetails from "./components/EmployeeFulldetails/EmployeeFullDetails";
import { Component } from "react";
import ProtectedRoute from "./components/ProtectedRoute";
import Calender from "./components/Calender/Calender";
import Register from "./components/signup/register";
import Forgot from "./components/forgot/forgot";
import PaySlip from "./components/PaySlip/payslip";

class App extends Component {
  state = {
    user_jwt_token: "",
  };
  user_jwt_token_updater = (token) => {
    this.setState({ user_jwt_token: token });
  };
  render() {
    const { user_jwt_token } = this.state;
    return (
      <TimeSheetContext.Provider
        value={{
          user_jwt_token,
          user_jwt_token_updater: this.user_jwt_token_updater,
        }}
      >
        <Router>
          <Switch>
            <Route exact path="/login" component={Login} />
            <Route exact path="/admin" component={Admin} />
            <Route exact path="/hr" component={Hr} />
            <Route exact path="/forgot" component={Forgot} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/employee" component={Employee} />
            <ProtectedRoute
              exact
              path="/employeedetails/:id"
              component={EmployeeFullDetails}
            />
            <ProtectedRoute exact path="/profile" component={EmployeHome} />
            <ProtectedRoute exact path="/" component={Home} />
            <ProtectedRoute
              exact
              path="/filltimesheet"
              component={Filltimesheet}
            />
            <ProtectedRoute
              exact
              path="/allemployeesview"
              component={AllEmployeesView}
            />
            {/* <ProtectedRoute exact path='/timesheet' component={Timesheet}/> */}
            <ProtectedRoute exact path="/timesheet" component={Calender} />
            <ProtectedRoute exact path="/payslips" component={PaySlip} />
            <Route path="/not-found" component={NotFound} />
            <Redirect to="/not-found" />
          </Switch>
        </Router>
      </TimeSheetContext.Provider>
    );
  }
}

export default App;
