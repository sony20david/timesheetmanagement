//importing component from reack package
import { Component } from "react";
//importing css properties from employeehome.css file
import "./EmployeeFullDetails.css";
//importing header component
import Header from "../Header/header";
//importing sidebar component
import Sidebar from "../Sidebar/sidebar";
import Cookies from "js-cookie";
import { Redirect } from "react-router-dom";
import EmployeeTimesheetTable from "../EmployeeTimesheetTable/employeetimesheet";

{
  /**
   *Inheritance uses the EmployeeFullDetails extends to allow any component
   *to use the properties and methods of another component connected with the parent.
   */
}
class EmployeeFullDetails extends Component {
  state = {
    profileData: {},
    timesheetData: [],
  };

  componentDidMount() {
    this.profileApicall();
    this.timesheetApiCall();
  }

  timesheetApiCall = async () => {
    const { match } = this.props;
    const { params } = match;
    const { id } = params;
    //getting jwt_token from cookies and store it in token variable
    const token = Cookies.get("jwt_token");
    //Parse a string (written in JSON format) and return a JavaScript object of empid
    const employeeid = JSON.parse(token).emp_id;
    //Parse a string (written in JSON format) and return a JavaScript object of jwt_token
    const jwt_token = JSON.parse(token).jwt_token;
    //intializing url with path,option method "get",accepting headers content-type json,
    //Authorization of login user
    const url = `http://localhost:3001/get_specific_timesheet/${id}`;
    const option = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${jwt_token}`,
      },
    };

    /**
     *It is an asynchronus function to handle fetch call
     *it sets State of month_days iterating of each item and push that data into month_days array
     *Storing the information in localstorage "lastname",converting json data to string.
     */
    const response = await fetch(url, option);
    const data = await response.json();
    this.setState({ timesheetData: data });
  };

  iteratingTableData = (year,id)=>{
    const{timesheetData} = this.state
    return(
    timesheetData.map((each) => (
      <EmployeeTimesheetTable each={each} key={each.id} year={year} id={id} />
    ))
    )
  }

  profileApicall = async () => {
    const token = Cookies.get("jwt_token");
    //Parse a string (written in JSON format) and return a JavaScript object
    const cookies_data = JSON.parse(token);
    const { match } = this.props;
    const { params } = match;
    const { id } = params;
    //intializing url with path,option method "get",accepting headers content-type json
    const url = `http://localhost:3001/get_specific_emp/${id}/employee`;
    const option = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${cookies_data.jwt_token}`,
      },
    };

    /**
     *It is an asynchronus function to handle fetch call
     *it sets State of employee data like name,date of joining etc..
     *Storing the information in localstorage "lastname",converting json data to string.
     */
    const resonse = await fetch(url, option);
    const data = await resonse.json();
    this.setState({ profileData: data[0] });
  };

  render() {
    //redndering EmployeeFullDetails component
    const { profileData, timesheetData } = this.state;
    const no_timesheet_data = timesheetData.length === 0;
    //initializing array with months
    const monthNames = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    //creating date object with the new Date() constructor.
    const d = new Date();
    //getUTCMonth() returns the month (0 to 11) of a date.
    //initializing monthnames using getUTCMonth()
    const month = monthNames[d.getUTCMonth()];
    //Get the year as a four digit number (yyyy)
    const year = d.getFullYear();
    const { match } = this.props;
    const { params } = match;
    const { id } = params;
    const female_profile =
      "https://png.pngitem.com/pimgs/s/95-953381_work-profile-user-default-female-suit-female-profile.png";
    const male_profile =
      "https://pngset.com/images/male-professional-avatar-icon-logo-symbol-trademark-stencil-transparent-png-853131.png";
    const token = Cookies.get("jwt_token");
    const role = JSON.parse(token).role;
    if (role !== "hr") {
      return <Redirect to="/not-found" />;
    }
    return (
      <>
        <Header />
        <div className="d-flex flex-row">
          <Sidebar />
          <div className="card4 d-flex flex-row">
            <div className="card2 d-flex flex-column">
              <div>
                {profileData.gender === "male" ? (
                  <img className="pic" src={male_profile} alt="timesheet" />
                ) : (
                  <img className="pic" src={female_profile} alt="timesheet" />
                )}
              </div>
              {/* <hr className="line"></hr> */}
              <h6>Name: {profileData.name}</h6>
              <p>EmployeeId: {profileData.empid}</p>
              <p>YearofJoining: {profileData.yearofjoining}</p>
              <p>Phoneno: {profileData.phoneno}</p>
              <p>Role: {profileData.role}</p>
              <p>Email: {profileData.email}</p>
              <p>City: {profileData.city}</p>
              <p>District: {profileData.district}</p>
              <p>State: {profileData.state}</p>
              <p>Country: {profileData.country}</p>
              <p>BloodGroup: {profileData.bloodgroup}</p>
              <p>Position: {profileData.position}</p>
              <p>Gender: {profileData.gender}</p>
            </div>
            <div className="card3">
              <h3 className="h3">
                {month} {year}
              </h3>
              {no_timesheet_data ? (
                <div className="div-timesheet">
                  <img
                    src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUSFRESERYSDxASEhIYERISEhIREhERGRoZGhgUGBgcIS4lHB4sHxgYJjgnKy80NTU1GiQ7QDszPzw0NTEBDAwMEA8QHhISHjEsISE0NDY0NDQ4NjQ1NDQ0MTQ0NDQxNDQxNDQ0NDQ0NDQxNDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NP/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAAAQMCBAYFBwj/xABFEAACAQICBAgMBAQEBwAAAAAAAQIDEQQSEyExUQUGIkFhcaHwBxQWMlJTgZGSwdHSFUKTsSNzgqI0crLxJGJjZKPC4f/EABoBAQADAQEBAAAAAAAAAAAAAAABAgMEBQb/xAAvEQEAAgEACQMDAgcAAAAAAAAAAQIRAwQTITFBUWGREjJSIqHRgcEFFCNCcbHw/9oADAMBAAIRAxEAPwD7MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIQsBIIsLASCLCwEgiwsBIIsLASCLACQQLASCLCwEgiwsBIIsLASCLCwEgiwAkAAAAAAAEIBET2PqYGOlXdEaWPdGvGTJzMC/Sx7onSooU2MzAv0qI0se6Ks7MczAv0se6J0q7o18zF7261+4G4QSQARJCJAr0q7ojSx7oqhz6zL29gGelj3ROlRWuvsJ/q7AMtLHuhpY90Y/1dhD6+wDPSx7oaWPdGHt7DCfXcDaDIjsXUSwJAAAAAAABCImtT6iURLY+oDWjC5loukypbDMCrRdJLo9JYZPYBVoekh0jYMJAU6LpMZQs11r5F5XU2rr+gGwQSQARJCJA1YLbqTMrPcipzjFNydlvNFudTTaOThFwlGnJ3sp21St1lLXiJiOcr1pMxM8obdXG0oNqc6MWnZqVSMWnus2fO+NnhJnRrOjg4Upxp2z1ailJTlzqKTXJ2cq+v8Af57xi4v1sHVlHEJTk3d1opyhUlK9+U1tunqes0sRiVUjC8bVIpJyT1Sitl1vOilaTWZmd/L93Ne94mIxu/7D9AcTuG5Y/C08ROmqU3KcZRTbi3GTV435nu5tZ7e3WkmnzrYcT4O+FqdXD0MLRTkqOEj4xLLJaPEOTTg29Us3Llq2JLedTwa0ouDvmhJpmFrYvFeuW9YzSZ6Y+7ds9yMZroSJ1dJjK3SWQ2Y7F1EsiOxdRLAkAAAAAAAEIiWx9RKMZyS2gV0thGIqqnGU5ebFNu23qRjCVjW4TlelVVvySfu1/ImsZmIRacRMvKfGyndxUJSalleVTnyuaN4wavqeroMJccqMUs0XFSTyt6RZle11eGvWedR4IU5SdNTvnU5ZKk4LOr2lZNK+0T4ruWVSp1JKN8qlUk8qbu7crVrdz09lqkT9X+97g2mszG7Hh6C48Yfu5/aHx4w/dz+083yRj6mf6j+4eSMfUz/Uf3F9lqHfzH5U2mudvEvS8tsP3c/tK5cc8O7fWf2mj5Ix9TP9R/cPJGPqZ/qP7hstQ6z5g9eudvEvT8uKHdy+0jy4w/dz+083yRj6mf6j+4eSMfUz/Uf3DZah38wnaa528S9Ly3od3P7R5b0O7n9p5vkjH1M/1H9w8kY+pn+o/uGy1Dv5j8m01zt4l6H4nGrarrqU9doxb5tq2Xv7DFcc6C1ebbVblK3RbJqKsHwDOjfR05xUtqz3T6bN7Tk/CIpUnh+To6k1UlJ8m8orKlffrb2nDoNX0ddPNJj1VtM4tE747Wjp0mHXptNpLaGLROLVjhMTie8T9971eMCp8J0qtOlNKo5KcE21yo7L6leLu1eztc8Hih4Op1qjljXGFKF70YVFKpUfS4vkR17dr6DmcRwhOcYpfwmk1KVOU4yne23Xq2bEWcW+E5YCv4zRUZTdOcJKd3GcJNNp217Yp+w0v/D7UvOxn6Z34njlnXXYvSNrH1RuzHDD7vwLwNSwdPRYaEaULtuzcpSlscpSeuT1LbuK8PP+PPK7qSd2tl1b5nLcUuM+I4QVaE8inBxfIi4pQkmrPW+dM7LB0VTWpXk/Olv6F0Hm6al403omPbO+f05dXforV2fqifdwj9ebZs95hUT52ZaToMKk7rcas2zHYuolkR2LqJYEgAAAAAAAhFdWLdrFiJA1cj3FWMpNwqK22Ev2ZsYmrkSsk23zmnica1F6o/lXPsbSfY2THFE8Gjxclyp9MYvt/wDp0JzPFuXLtvp/tlOmN9ZjGklnoPYAA52oQSaeIlLPCMZKCcZtuybunGy19bJhEzhtg1dDU9Z/44/U18Pi5qejqxd8zUZxTUJanJXvzuO5vWnsGEerq9IkgkhYOS8IXA9PEYSrUkv4uGhOpTmtqSV5Re+LS7EdaafClDSUa9P1lKpH4otfMtS00tFo5K3r6qzD84EMlFmGjecFvnBe9o+ieLD7VxL4tLBUbvl16yjKpPmStdQj0K762dHke42IqyS3IyPnbXm8za3GXtVrFYxDVyPcYTpSfMboKrIQZJDAkAAAAAAAEIkhEgaGOlrity/f/Y1KkFJNN2T59x67gntSfsKZ0Izi4tK0otOyV7NWEEuf4CdqqXRNe7/Y6k5fA6sRbdUqLtkdQdGs+6J6xDHQe3HcABztg1K+qdLpzx96v/6m2auL86i/+pb3wkiYRLHD1ZPPFxmnTaipSUUqvJTzRs9l3bXuKI1JTjRqSjODlUi8k0lOCacbSs2nt7S+OCjtmtJJ7ZS16+hflXQiqvRyR1NuOkpOMW28vLimk3rt0cxOY5K78b3oEgFVw1eEcVGjSq1p+bTpznLqimzaOH8KnCOjwsaKdpYiaT36OHKl25V7QiZxD5bLHxcnN4ejrWuPLtdu99u0PHwvFxoUYuMlJNZ+bm2miDq/mNL8nNsqdH6H4D4QWJw9CutWlpwk1ulblL2O56Jz/EWGXAYPppKXsk3JfudAcrqAAAIZJDAkAAAAAAAEIkhBsCTC1muohy3WXWY3e9Ac95uKl/OX9yT+Z05y+O5OJbfp0n2RXyOoOjT74pPZjov7o7gAOdsGninF2i5qEk4yWuKknrtqe+z9zNw8nhHgOliJqpUzZlFR5MrJxWbU+vM/ci1cZ+qcfdW2cboW6WNlLxjkSdoyvRyye5O2tmNWUG1CVdXcklFukm5RaeXZe97e816nAFOVPROU2s8puV45m5RcGvNslldtS6dop8XaUbtSqXc5Tu5Xam4zi5K61O0v7UX/AKfX7Qz+vpHmXr05qSTi1JPY00010NFhpcG4COHgqcHJwTbipPNlvraT3Xu/abpnOIndwaxnG/iHxvwpY7SYxU0+TQpRjbdOfLl2ZPcfYz888YsTpcViqm3PXqW/yqTjHsSFeKLcHmydrvcGzOWGlKnWqR8ykqbm/wDPOMIr3vsZhLY+ouzfoXi9RcMLhIKyy4aivdCJu8rmat0mHB6tSpLdTh/pRfT2GbZQnLNa6vb2W+ptFH5/6PmXgCGSQwJAAAAAAABCIkSiJAa9TaYosna+u5CUekDnuFtVVv8A5Kb7ZfQ6NYqD154fEjyuFeD3UlGcGm8uVxbS1Jtpr3s8/wDC6nor4o/U68U0lK5tjDnzatpxGcul8Zh6cPiiT4zD04fFE5n8Lqeivij9R+F1PRXxR+pGw0fzTtL/ABdN4zD04fFEeMw9OHxROZ/DKnor4o/Uj8Lqeivij9RsNH8zaX+Lp/GYenD4ojxmHpw+KJzT4LqpXyq3+aP1MPw6for4ojYaP5m0v8XUeMw9OHxRHjMPTh8UTl/w6p6K+KJYuCKr15F8UfqNho/mbS/xdFLEws+XDY/zRPzhKV229rbb62fbvwir6C+KP1PjnDGDlQr1qMlllCclbo2xfwtFLUrX22yeq08Yw9zgbCxnwZwpJuOfPh7JtXtTlGer3nLS2PqOs4n0JVqOPoQSb0Tna6T82UdW/WonJrWusrMRERiU5fo3g7lUqLu9dKm/7UWKinru11M0uLdZSwmDlfzsNQftyRN+NRLU2kZNlapcq13svt17TaNdTWa91bLb23NgAQySGBIAAAAAAAIREjIxkBr1NpiWTlZ8xCn0IDAFmkW4aRbgKwWaRbhpFuArBnnW4jP0L3ATV8z2I1Dcq+a+pGmAPRp7I9SPOPRpebHqQGR8o8K/BeSrTxUVyasck/5kNcX7Y/6D6weHxo4JWMw1ehbltZqT9GrHXB9GvV1NkwiYzD5V4OsXo8dTi/NrQnSlueZZo/3QXvOdxdHJOpT9XOpD4ZOPyJw1aVGpTqJOM6VSE7bGpwknZ+1WN/jRFLF4px8ydRzhuyVEprsmXZ8n2biW74HBP/t6a9yt8j2IxT2pN9R4fEeX/AYLU3/Bj+7PZVS2rLJ9KRm1jgjIs1rK2XtubJqKpyr5Xsta2vbtNsAQSAAAAAAACABJjIkMCicXcwyPcXTg+Z2MdHL0uwCvI9wyPcWxhK+uV1usWZQNbI9wyPcbOUZQNbI9wyPcbOUOIFFXzH1I1DfnTumirxXp7ANU9Gl5sepFHivT2F8FZJbkBmVx2szIUdvSB8Z8JPA3i+JdWCtSxN5atkav54+3VL2s5fF18+jf5o0qcJdORZI/2xifceNnAfjmHnSWVzcqcqbldKMovXrWy8XJe0+ScZuCo4PRwlKnpnKWenHzorml1Oz29BpX0+nM238o35n9o/VlMW9W6N3Oen5fWuI3+Awf8lfuz3aew8PiP/gMF/Jj8z3UrGbWOCr8/wDR8y8rycrN0WsZgSCABIIAEggASAAABhJPmYGYKXF92MrAtuRdb+0qs9zIsBddb+0ZlvKbAC7Mt5GZbyoAXZlvGdFIAuzoZ0UgC7OhmW8pAFPCmL0VGrUUKldwptqnSi51Ju2qMYrW2z5FR4rU4yliOFa0KGJrTdSOFdWMXCMrtOcpa3bUsq1LVdvYvr9eDnGUVKVNyjJKcbZoNq2aN01dbdaOGxXgvpVpOdbF46tUe2c5UZSfRdweroE5xMROEc4mYz/l2XAeGpUqFONBqVJpSjLNnUlPlZlLnTvqPSueZwTwesNQo4eDnONGnCEZTtmlGKsr2VjdyvcTMzM5mcyRERGI4LwU5X0hRfdkJWkmCT3mYAAAAAAAAAAAAAAAAAAAAABFhYkARYWJAEEgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf/Z"
                    className="image1"
                  />
                  <h4 className="timesheet-head">
                    No! timesheet data found for{" "}
                    <span className="span1">{profileData.name}</span> in {month}
                  </h4>
                </div>
              ) : 
                <ul className="timesheet-ul">
                  {this.iteratingTableData(year,id)}
                </ul>
              }
            </div>
          </div>
        </div>
      </>
    );
  }
}
//exporting EmployeeFullDetails component
export default EmployeeFullDetails;
