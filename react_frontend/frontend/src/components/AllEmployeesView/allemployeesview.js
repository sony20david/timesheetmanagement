import Cookies from "js-cookie";
import { Component } from "react";
import { Redirect } from "react-router-dom";
import EmployeeCard from "../EmployeeCard/employeecard";
import Header from "../Header/header";
import Sidebar from "../Sidebar/sidebar";
import "./allemployeesview.css";

class AllEmployeesView extends Component {
    state = {
        employeeList: [],
    };

    componentDidMount = () => {
        this.employeeDataApiCall();
    };

    employeeDataApiCall = async () => {
        const token = Cookies.get("jwt_token");
        const jwt_token = JSON.parse(token).jwt_token;
        const url = "http://localhost:3001/get_profiles";
        const option = {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${jwt_token}`,
            },
        };
        const response = await fetch(url, option);
        const data = await response.json();
        this.setState({
            employeeList: data,
        });
    };

    render() {
        const { employeeList } = this.state;
        const token = Cookies.get("jwt_token");
        const role = JSON.parse(token).role;
        if (role === "employee") {
            return <Redirect to="/not-found" />;
        }
        return (
            <>
                <Header />
                <div className="allemployees">
                    <Sidebar />
                    <ul className="employeecards">
                        {employeeList.map((each) => (
                            <EmployeeCard each={each} key={each.id} />
                        ))}
                    </ul>
                </div>
            </>
        );
    }
}
export default AllEmployeesView;
