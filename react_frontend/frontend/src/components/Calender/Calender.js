//importing react,component from react package
import React, { Component } from "react";
//importing css properties from calender.css file
import './Calender.css'
//importing link,redirect from react-router-dom package
import { Link, Redirect } from "react-router-dom";
//importing header component 
import Header from "../Header/header";
//importing sidebar component 
import Sidebar from "../Sidebar/sidebar";
//importing cookies from js-cookie package
import Cookies from "js-cookie";
import { setTimesheetData } from "../../redux/actions/authenticationActions";
import store from '../../redux/store';

/**
  *Inheritance uses the Calender extends to allow any component 
  *to use the properties and methods of another component connected with the parent. 
*/
class Calender extends Component {
  //setting state timesheetdata,dates with empty array
  constructor(props) {
    super(props)
    this.state = {
      timeSheetData: [],
      dates: []
    }
    store.subscribe(() => {
      this.setState({
        timeSheetData: store.getState().employeeTimesheet.timesheetData
      })
    })
  }


  //componentDidMount() is invoked immediately after a component is mounted.
  componentDidMount = () => {
    //it calls the apicall function
    this.timesheetApiCall()
  }

  //It is an asynchronus call to get "jwt_token",fetching data
  timesheetApiCall = async () => {
    //getting jwt_token from cookies and store it in token variable
    const token = Cookies.get("jwt_token")
    //Parse a string (written in JSON format) and return a JavaScript object of empid
    const employeeid = JSON.parse(token).emp_id
    //Parse a string (written in JSON format) and return a JavaScript object of jwt_token
    const jwt_token = JSON.parse(token).jwt_token
    //intializing url with path,option method "get",accepting headers content-type json,
    //Authorization of login user
    const url = `http://localhost:3001/get_specific_timesheet/${employeeid}`
    const option = {
      method: "GET",
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "Authorization": `Bearer ${jwt_token}`
      }
    }

    /**
      *It is an asynchronus function to handle fetch call
      *it sets State of month_days iterating of each item and push that data into month_days array
      *Storing the information in localstorage "lastname",converting json data to string.
   */
    const response = await fetch(url, option)
    const data = await response.json()
    store.dispatch(setTimesheetData(data));
    let month_days = []
    //Iterating each item of data
    for (let item of data) {
      //adding items in month_days
      month_days.push(item.day)
    }
    //setting state with timesheetdata,dates
    this.setState({ dates: month_days })
  }

  //getting jwt_token from cookies
  render() {
    const token = Cookies.get("jwt_token")
    ///Parse role (written in JSON format) and return a JavaScript object
    const role = JSON.parse(token).role
    //initializing array with months
    const monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    //creating date object with the new Date() constructor.
    const d = new Date();
    //getUTCMonth() returns the month (0 to 11) of a date.
    //initializing monthnames using getUTCMonth()
    const month = monthNames[d.getUTCMonth()]
    //Get the day as a number (1-31)
    const date = d.getDate();
    //Get the year as a four digit number (yyyy)
    const year = d.getFullYear();
    const { dates } = this.state
    if (role === "admin") {
      return <Redirect to="/not-found" />
    }
    return (
      <>
        <Header />
        <div className="d-flex flex-row">
          <Sidebar />
          <div className="d-flex flex-column tables mt-4">
            <div className="d-flex flex-row">
              <div className="header1 m-2">
                <h4>Work from home</h4>
              </div>
              <div className="header2 m-2">
                <h4>Holidays</h4>
              </div>
              <div className="header3 m-2">
                <h4>Leave</h4>
              </div>
              <div className="header4 m-2">
                <h4>Present days</h4>
              </div>
            </div>

            <h4>{date} {month} {year}</h4>
            <div>
              <table style={{ width: "100%" }}>
                <tbody>
                  <tr>
                    <th>Monday</th>
                    <th>Tuesday</th>
                    <th>Wednesday</th>
                    <th>Thursday</th>
                    <th>Friday</th>
                    <th>Saturday</th>
                    <th>Sunday</th>
                  </tr>
                  <tr>
                    <td>

                    </td>
                    <td>
                      {date >= 1 && !dates.includes(1) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 1,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">1</span>
                    </td>
                    <td>
                      {date >= 2 && !dates.includes(2) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 2,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">2</span>
                    </td>
                    <td>
                      {date >= 3 && !dates.includes(3) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 3,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">3</span>
                    </td>
                    <td>
                      {date >= 4 && !dates.includes(4) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 4,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">4</span>
                    </td>
                    <td>
                      {date >= 5 && !dates.includes(5) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 5,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">5</span>
                    </td>
                    <td>
                      {date >= 6 && !dates.includes(6) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 6,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">6</span>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {date >= 7 && !dates.includes(7) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 7,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">7</span>
                    </td>
                    <td>
                      {date >= 8 && !dates.includes(8) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 8,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">8</span>
                    </td>
                    <td>
                      {date >= 9 && !dates.includes(9) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 9,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">9</span>
                    </td>
                    <td>
                      {date >= 10 && !dates.includes(10) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 10,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">10</span>
                    </td>
                    <td>
                      {date >= 11 && !dates.includes(11) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 11,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">11</span>
                    </td>
                    <td>
                      {date >= 12 && !dates.includes(12) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 12,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">12</span>
                    </td>
                    <td>
                      {date >= 13 && !dates.includes(13) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 13,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">13</span>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {date >= 14 && !dates.includes(14) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 14,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">14</span>
                    </td>
                    <td>
                      {date >= 15 && !dates.includes(15) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 15,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">15</span>
                    </td>
                    <td>
                      {date >= 16 && !dates.includes(16) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 16,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">16</span>
                    </td>
                    <td>
                      {date >= 17 && !dates.includes(17) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 17,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">17</span>
                    </td>
                    <td>
                      {date >= 18 && !dates.includes(18) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 18,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">18</span>
                    </td>
                    <td>
                      {date >= 19 && !dates.includes(19) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 19,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">19</span>
                    </td>
                    <td>
                      {date >= 20 && !dates.includes(20) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 20,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">20</span>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {date >= 21 && !dates.includes(21) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 21,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">21</span>
                    </td>
                    <td>
                      {date >= 22 && !dates.includes(22) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 22,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">22</span>
                    </td>
                    <td>
                      {date >= 23 && !dates.includes(23) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 23,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">23</span>
                    </td>
                    <td>
                      {date >= 24 && !dates.includes(24) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 24,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">24</span>
                    </td>
                    <td>
                      {date >= 25 && !dates.includes(25) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 25,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">25</span>
                    </td>
                    <td>
                      {date >= 26 && !dates.includes(26) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 26,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">26</span>
                    </td>
                    <td>
                      {date >= 27 && !dates.includes(27) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 27,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">27</span>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {date >= 28 && !dates.includes(28) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 28,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">28</span>
                    </td>
                    <td>
                      {date >= 29 && !dates.includes(29) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 29,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1">29</span>
                    </td>
                    <td>
                      {date >= 30 && !dates.includes(30) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 30,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1 mb-1">30</span>
                    </td>
                    <td>
                      {date >= 31 && !dates.includes(31) ?
                        <Link to={{
                          pathname: '/filltimesheet',
                          day: 31,
                        }}>
                          <button className="btn3" />
                        </Link>
                        :
                        <button className="btn5" />
                      }
                      <span className="span1 mb-1">31</span>
                    </td>
                    <td />
                    <td />
                    <td />
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Calender;
