import "./header.css";
import Cookies from "js-cookie";
import { withRouter } from "react-router-dom";
// import { setInitialData } from "../../redux/actions/authenticationActions";
// import store from '../../redux/store';

const Header = (props) => {
  const onClickLogout = () => {
    // store.dispatch(setInitialData());
    localStorage.removeItem("persist:Storage");
    const { history } = props;
    Cookies.remove("jwt_token");
    // localStorage.removeItem("lastname")
    history.replace("/login");
  };
  const user_data = Cookies.get("jwt_token");
  const user_parse_data = JSON.parse(user_data);
  return (
    <div className="header-container  ">
      <div className="test">
        <input type="checkbox" className="menu-toggle" id="menu-toggle" />
        <div className="mobile-bar">
          <label htmlFor="menu-toggle" className="menu-icon menu">
            <span />
          </label>
        </div>
        <div className="header">
          <nav>
            <ul>
              {/* <Link to="/profile"> */}
              <li>
                <a href="/profile">Hello {user_parse_data.username}</a>
              </li>
              {/* </Link> */}
              <li>
                <a onClick={onClickLogout}>Logout</a>
              </li>
            </ul>
          </nav>
        </div>
        <a className="navbar-brand" style={{ padding: 0 }}>
          <img src="https://www.innominds.com/hubfs/logo%20-03.png" />
        </a>
      </div>
    </div>
  );
};

export default withRouter(Header);
