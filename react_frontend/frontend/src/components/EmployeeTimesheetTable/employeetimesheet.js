import "./employeetimesheet.css";

const EmployeeTimesheetTable = (props) => {
    // console.log(props)
    const{each,year,id} = props
    var hours = 0
    var description = ""
    console.log(each)
    // for(let item in each){
    //     console.log(item)
    // }
    Object.entries(each).forEach(item => {
        if(typeof(item[1]) === "object" && item[1].hours > 0){
            hours = hours + item[1].hours
            description = description +", "+item[1].description
        }
      })
    description = description.trim(",")
  return (
    <table>
      <tr>
        <th>EMPID</th>
        <th>DATE</th>
        <th>WORKEDHOURS</th>
        <th>DESCRIPTION</th>
        <th>FILLED</th>
      </tr>
      <tr>
        <td>{id}</td>
        <td>{each.day}-{each.month}-{year}</td>
        <td>{hours}</td>
        <td>{description}</td>
        <td>Yes</td>
      </tr>
    </table>
  );
};

export default EmployeeTimesheetTable;
