import React, { Component } from "react";
import "./forgot.css";

class Forgot extends Component {
  state = {
    email: "",
    security_question: "",
    new_password: "",
    position: "",
    err_msg: "",
    is_active: false,
  };

  changeEmail = (event) => {
    this.setState({ email: event.target.value });
  };
  changeSecurityQuestion = (event) => {
    this.setState({ security_question: event.target.value });
  };

  changePassword = (event) => {
    this.setState({ new_password: event.target.value });
  };

  passwordUpdated = (data) => {
    alert("password update successfully");
    this.setState({ is_login: false, err_msg: "" });
    const { history } = this.props;
    history.replace("/login");
  };

  updateFail = (event) => {
    this.setState({ is_active: true, err_msg: event.status_message });
  };

  changePosition = (event) => {
    this.setState({ position: event.target.value });
  };

  submitingResetData = async (event) => {
    event.preventDefault();
    const { email, security_question, new_password, position } = this.state;
    const userDetails = {
      email,
      security_question,
      new_password,
      position,
    };
    const url = `http://localhost:3001/forgot`;
    const option = {
      method: "PUT",
      body: JSON.stringify(userDetails),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, option);
    const data = await response.json();
    if (data.status_code === 200) {
      this.passwordUpdated(data);
    } else {
      this.updateFail(data);
    }
  };

  render() {
    const {
      email,
      security_question,
      newpassword,
      is_active,
      err_msg,
      position,
    } = this.state;
    // const jwtToken = Cookies.get('jwt_token')
    // if (jwtToken !== undefined) {
    //   return <Redirect to="/" />
    // }
    return (
      // <TimeSheetContext>
      //   {(value)=>{
      //     const{user_jwt_token_updater} = value
      //     if(jwt_token !== ""){
      //       user_jwt_token_updater(jwt_token)
      //     }
      //user_jwt_token_updater(jwt_token)

      <div className="form-row">
        <div className="card1-can">
          <div className="panel-employee-login position-relative">
            <div className=" py-4 px-4 rounded-top border-bottom text-center">
              <h1 className=" heading">Forgot password</h1>
            </div>
            <form className="p-4" onSubmit={this.submitingResetData}>
              <div className="form-group">
                <label htmlFor="exampleInputEmail1">email</label>
                <input
                  type="email"
                  className="form-control form-control-lg"
                  placeholder="Enter email"
                  onChange={this.changeEmail}
                  value={email}
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputSecurity">security_question</label>
                <input
                  type="text"
                  className="form-control form-control-lg"
                  placeholder="security_question"
                  onChange={this.changeSecurityQuestion}
                  value={security_question}
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputPassword1">New Password</label>
                <input
                  type="password"
                  className="form-control form-control-lg"
                  placeholder="Password"
                  onChange={this.changePassword}
                  value={newpassword}
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputPassword1">Position</label>
                <input
                  type="text"
                  className="form-control form-control-lg"
                  placeholder="position"
                  onChange={this.changePosition}
                  value={position}
                />
              </div>
              <div className="d-block text-center">
                <button type="submit" className="button1 badge-pill px-4">
                  Reset
                </button>
                {is_active && <p className="para1">* {err_msg}</p>}
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Forgot;
