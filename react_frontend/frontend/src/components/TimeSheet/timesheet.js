import React, { Component } from "react";
import { Link } from "react-router-dom";
import Header from "../Header/header";
import Sidebar from "../Sidebar/sidebar";
import "./timesheet.css";

class Timesheet extends Component {
  render() {
    return (
      <>
        <Header />
        <div className="d-flex flex-row">
          <Sidebar />
          <div className="d-flex flex-column tables mt-4">
            <div className="d-flex flex-row">
              <div className="header1 m-2">
                <h4>Work from home</h4>
              </div>
              <div className="header2 m-2">
                <h4>Holidays</h4>
              </div>
              <div className="header3 m-2">
                <h4>Leave</h4>
              </div>
              <div className="header4 m-2">
                <h4>Present days</h4>
              </div>
            </div>
            <h4>Feb 2022</h4>
            <div>
              <table style={{ width: "100%" }}>
                <tbody>
                  <tr>
                    <th>Monday</th>
                    <th>Tuesday</th>
                    <th>Wednesday</th>
                    <th>Thursday</th>
                    <th>Friday</th>
                    <th>Saturday</th>
                    <th>Sunday</th>
                  </tr>
                  <tr>
                    <td>
                      <button className="btn3" />1
                    </td>
                    <td>
                      <button className="btn3" />2
                    </td>
                    <td>
                      <button className="btn3" />3
                    </td>
                    <td>
                      <button className="btn3" />4
                    </td>
                    <td>
                      <button className="btn3" />5
                    </td>
                    <td>
                      <button className="btn3" />6
                    </td>
                    <td>
                      <button className="btn3" />7
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <button className="btn3" />8
                    </td>
                    <td>
                      <button className="btn3" />9
                    </td>
                    <td>
                      <button className="btn3" />
                      10
                    </td>
                    <td>
                      <button className="btn3" />
                      11
                    </td>
                    <td>
                      <button className="btn3" />
                      12
                    </td>
                    <td>
                      <button className="btn3" />
                      13
                    </td>
                    <td>
                      <button className="btn3" />
                      14
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <button className="btn3" />
                      15
                    </td>
                    <td>
                      <button className="btn3" />
                      16
                    </td>
                    <td>
                      <button className="btn3" />
                      17
                    </td>
                    <td>
                      <button className="btn3" />
                      18
                    </td>
                    <td>
                      <button className="btn3" />
                      19
                    </td>
                    <td>
                      <button className="btn3" />
                      20
                    </td>
                    <td>
                      <button className="btn3" />
                      21
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <button className="btn3" />
                      22
                    </td>
                    <td>
                      <button className="btn3" />
                      23
                    </td>
                    <td>
                      <button className="btn3" />
                      24
                    </td>
                    <td>
                      <button className="btn3" />
                      25
                    </td>
                    <td>
                      <button className="btn3" />
                      26
                    </td>
                    <td>
                      <button className="btn3" />
                      27
                    </td>
                    <td>
                      <button className="btn3" />
                      28
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <button className="btn3" />
                      29
                    </td>
                    <td>
                      <button className="btn3" />
                      30
                    </td>
                    <td />
                    <td />
                    <td />
                    <td />
                    <td />
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Timesheet;
