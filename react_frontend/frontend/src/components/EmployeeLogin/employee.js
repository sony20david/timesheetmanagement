//importing React,component from react package
import React, { Component } from "react";
//importing css properties from employee.css file
import "./employee.css";
//importing cookies from js-cookies package
import Cookies from "js-cookie";
//importing link,redirect form react-router-dom package
import { Link, Redirect } from "react-router-dom";
import {
  setLoginData,
  setProfileData,
} from "../../redux/actions/authenticationActions";
import store from "../../redux/store";

/**
 *Inheritance uses the Employee extends to allow any component
 *to use the properties and methods of another component connected with the parent.
 *getting "jwt-token" from cookies if jwtToken is not undefined then redirect to register page
 */
class Employee extends Component {
  //setting state with username,password,is_login,err_msg,jwt_token with empty
  state = {
    username: "",
    password: "",
    is_login: false,
    err_msg: "",
    jwt_token: "",
  };

  //It is an asynchronus function which will occur any event happens
  changeUsername = (event) => {
    this.setState({ username: event.target.value });
  };

  //It is an asynchronus function which will occur any event happens
  changePassword = (event) => {
    this.setState({ password: event.target.value });
  };

  //It ia an asynchronus function to handle loginSuccess
  loginSuccess = (data) => {
    this.setState({ is_login: false, err_msg: "" });
    const { history } = this.props;

    //setting jwt-token
    Cookies.set("jwt_token", JSON.stringify(data), {
      expires: 30,
      path: "/",
    });
    //if everything satisfies condition goes to home page.
    history.push("/");
  };

  //It is an asynchronus function to handle if status_code is not 200
  //it gives error message if login fails
  loginFailure = (event) => {
    this.setState({ is_login: true, err_msg: event.status_message });
  };

  //this is an asynchronus function to handle the submitting data
  submitingLoginData = async (event) => {
    //if any event happens without entering the data then preventDefault will take care of that
    event.preventDefault();
    //setting state with entered username,password by user
    const { username, password } = this.state;
    const userDetails = {
      username,
      password,
    };
    //setting path to url,method type
    const url = "http://localhost:3001/login/user";
    const option = {
      method: "POST",
      //converting json data into string type,headers will accept content-type:json
      body: JSON.stringify(userDetails),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };

    /**
     *fetching url,option if data.status_code is 200 then goes to loginSuccess function
     *else it goes to loginFailure function
     */
    const response = await fetch(url, option);
    const data = await response.json();
    //intializing url with path,option method "get",accepting headers content-type json
    const url1 = `http://localhost:3001/get_specific_emp/${data.emp_id}/${data.role}`;
    const option1 = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${data.jwt_token}`,
      },
    };

    /**
     *It is an asynchronus function to handle fetch call
     *it sets State of employee data like name,date of joining etc..
     *Storing the information in localstorage "lastname",converting json data to string.
     */
    const resonse1 = await fetch(url1, option1);
    const data1 = await resonse1.json();
    store.dispatch(setLoginData(data));
    store.dispatch(setProfileData(data1[0]));
    if (data.status_code === 200) {
      this.loginSuccess(data);
    } else {
      this.loginFailure(data);
    }
  };

  //getting "jwt-token" from cookies if jwttoken is not equal to undefined goes to home
  //setting state with username,password,is_login,err_msg
  render() {
    const { username, password, is_login, err_msg } = this.state;
    const jwtToken = Cookies.get("jwt_token");
    if (jwtToken !== undefined) {
      return <Redirect to="/" />;
    }
    //rendering employee login component
    return (
      <div className="form-row">
        <div className="login-form-container card2-can">
          <img
            src="https://lidac.com/images/2-individual_employee_login.jpg"
            class="logo1"
            alt="website"
          />
        </div>
        <div className="card1-can">
          <div className="panel-employee-login position-relative">
            <div className=" py-4 px-4 rounded-top border-bottom text-center">
              <h1 className=" heading">Employee Login</h1>
            </div>
            {/**
             * on clicking submit button it goes to submitingLoginData function to verify details
             */}
            <form className="p-4" onSubmit={this.submitingLoginData}>
              <div className="form-group">
                <label htmlFor="exampleInputEmail1">Username</label>
                <input
                  type="text"
                  className="form-control form-control-lg"
                  placeholder="Enter username"
                  //ontyping the username it goes to changeUsername function
                  onChange={this.changeUsername}
                  value={username}
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputPassword1">Password</label>
                <input
                  type="password"
                  className="form-control form-control-lg"
                  placeholder="Password"
                  //ontyping password it goes to changePassword function
                  onChange={this.changePassword}
                  value={password}
                />
              </div>
              <div className="row">
                <div className="col-6">
                  <div
                    className="form-group form-check"
                    style={{ display: "none" }}
                  >
                    <input
                      type="checkbox"
                      className="form-check-input"
                      id="exampleCheck1"
                    />
                    <label className="form-check-label" htmlFor="exampleCheck1">
                      Check me out
                    </label>
                  </div>
                </div>
                <div className="col-6 text-right">
                  {/**
                   * if user clicks on forgot it goes to forget password page.
                   */}
                  <Link to="/forgot">
                    <button
                      type="button"
                      className="reset-password-btn btn btn-link heading p-0"
                    >
                      Forgot Password?
                    </button>
                  </Link>
                </div>
              </div>
              <div className="d-block text-center">
                <button type="submit" className="button1 badge-pill px-4">
                  Login
                </button>
                {/**
                 * It shows the login success or login fails message based on true or false
                 */}
                {is_login && <p className="para1">* {err_msg}</p>}
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

//exporting Employee component
export default Employee;
