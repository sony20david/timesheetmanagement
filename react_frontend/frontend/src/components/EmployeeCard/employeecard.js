import { Link } from "react-router-dom";

import "./employeecard.css";

const EmployeeCard = (props) => {
  const female_profile =
    "https://png.pngitem.com/pimgs/s/95-953381_work-profile-user-default-female-suit-female-profile.png";
  const male_profile =
    "https://pngset.com/images/male-professional-avatar-icon-logo-symbol-trademark-stencil-transparent-png-853131.png";
  const { each } = props;
  const { name, empid, email, phoneno, role, gender } = each;
  return (
    <div className="card40 mb-3 mr-3">
      <div className="profile_pic">
        {gender === "male" ? (
          <img className="pic" src={male_profile} alt="timesheet" />
        ) : (
          <img className="pic" src={female_profile} alt="timesheet" />
        )}
      </div>
      <div className="desc">Name: {name}</div>
      <div className="desc">Empid: {empid}</div>
      <div className="desc">Email: {email}</div>
      <div className="desc">PhoneNo: {phoneno}</div>
      <div className="desc">Role: {role}</div>
      <hr className="line" />
      <div className="footer">
        <Link to={`/employeedetails/${empid}`}>
          <button className="btn-more">MORE</button>
        </Link>
      </div>
    </div>
  );
};
export default EmployeeCard;
