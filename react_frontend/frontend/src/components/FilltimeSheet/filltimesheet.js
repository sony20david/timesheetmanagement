import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import Header from "../Header/header";
import Sidebar from "../Sidebar/sidebar";
import "./filltimesheet.css";
import Cookies from "js-cookie";

class Filltimesheet extends Component {
  state = {
    documentation_presentation_hours: "",
    documentation_presentation_description: "",
    training_learning_hours: "",
    training_learning_description: "",
    proposals_technical_hours: "",
    proposals_technical_description: "",
    hiring_interviewing_hours: "",
    hiring_interviewing_description: "",
    delivery_hours: "",
    delivery_description: "",
    appraisals_hours: "",
    appraisals_description: "",
    mentoring_hours: "",
    mentoring_description: "",
    meeting_customers_hours: "",
    meeting_customers_description: "",
    is_error: false,
    err_msg: "",
  };
  changeDocumenthours = (event) => {
    this.setState({ documentation_presentation_hours: event.target.value });
  };
  changeDocumentdescrip = (event) => {
    this.setState({
      documentation_presentation_description: event.target.value,
    });
  };
  changeTraininghours = (event) => {
    this.setState({ training_learning_hours: event.target.value });
  };
  changeTrainingdescrip = (event) => {
    this.setState({ training_learning_description: event.target.value });
  };
  changeProposalhours = (event) => {
    this.setState({ proposals_technical_hours: event.target.value });
  };
  changeProposaldescrip = (event) => {
    this.setState({ proposals_technical_description: event.target.value });
  };
  changeHiringhours = (event) => {
    this.setState({ hiring_interviewing_hours: event.target.value });
  };
  changeHiringdescrip = (event) => {
    this.setState({ hiring_interviewing_description: event.target.value });
  };
  changeDeliveryhours = (event) => {
    this.setState({ delivery_hours: event.target.value });
  };
  changeDeliverydescrip = (event) => {
    this.setState({ delivery_description: event.target.value });
  };
  changeAppraisalhours = (event) => {
    this.setState({ appraisals_hours: event.target.value });
  };
  changeAppraisaldescrip = (event) => {
    this.setState({ appraisals_description: event.target.value });
  };
  changeMentoringhours = (event) => {
    this.setState({ mentoring_hours: event.target.value });
  };
  changeMentoringdescrip = (event) => {
    this.setState({ mentoring_description: event.target.value });
  };
  changeMeetinghours = (event) => {
    this.setState({ meeting_customers_hours: event.target.value });
  };
  changeMeetingdescrip = (event) => {
    this.setState({ meeting_customers_description: event.target.value });
  };

  alerting = () => {
    alert("data added successfully");
  };

  timeSheetFillSuccess = (data) => {
    this.setState({
      documentation_presentation_hours: "",
      documentation_presentation_description: "",
      training_learning_hours: "",
      training_learning_description: "",
      proposals_technical_hours: "",
      proposals_technical_description: "",
      hiring_interviewing_hours: "",
      hiring_interviewing_description: "",
      delivery_hours: "",
      delivery_description: "",
      appraisals_hours: "",
      appraisals_description: "",
      mentoring_hours: "",
      mentoring_description: "",
      meeting_customers_hours: "",
      meeting_customers_description: "",
      is_error: false,
      err_msg: "",
    });
    this.alerting();
  };

  timeSheetFillFail = (data) => {
    this.setState({ is_error: true, err_msg: data.status_message });
  };

  submittingData = async (event) => {
    event.preventDefault();
    let {
      documentation_presentation_hours,
      documentation_presentation_description,
      training_learning_description,
      training_learning_hours,
      mentoring_description,
      mentoring_hours,
      appraisals_description,
      appraisals_hours,
      delivery_description,
      delivery_hours,
      proposals_technical_description,
      proposals_technical_hours,
      hiring_interviewing_description,
      hiring_interviewing_hours,
      meeting_customers_description,
      meeting_customers_hours,
    } = this.state;
    if (documentation_presentation_hours === "") {
      documentation_presentation_hours = 0;
    }
    if (training_learning_hours === "") {
      training_learning_hours = 0;
    }
    if (proposals_technical_hours === "") {
      proposals_technical_hours = 0;
    }
    if (hiring_interviewing_hours === "") {
      hiring_interviewing_hours = 0;
    }
    if (delivery_hours === "") {
      delivery_hours = 0;
    }
    if (appraisals_hours === "") {
      appraisals_hours = 0;
    }
    if (mentoring_hours === "") {
      mentoring_hours = 0;
    }
    if (meeting_customers_hours === "") {
      meeting_customers_hours = 0;
    }
    const total_hours =
      parseInt(documentation_presentation_hours) +
      parseInt(training_learning_hours) +
      parseInt(proposals_technical_hours) +
      parseInt(hiring_interviewing_hours) +
      parseInt(delivery_hours) +
      parseInt(appraisals_hours) +
      parseInt(mentoring_hours) +
      parseInt(meeting_customers_hours);
    if (total_hours < 8 || total_hours > 24) {
      this.setState({
        is_error: true,
        err_msg: "you need to work minimum 8 hours and maximum 24 hours",
      });
    } else {
      const token = Cookies.get("jwt_token");
      const employeeid = JSON.parse(token).emp_id;
      const jwt_token = JSON.parse(token).jwt_token;
      const monthNames = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
      ];
      const d = new Date();
      const month = monthNames[d.getUTCMonth()];
      const { location } = this.props;
      const date = location.day;
      const body_details = {
        documentation_presentation: {
          hours: documentation_presentation_hours,
          description: documentation_presentation_description,
        },
        training_learning: {
          hours: training_learning_hours,
          description: training_learning_description,
        },
        proposals_technical: {
          hours: proposals_technical_hours,
          description: proposals_technical_description,
        },
        hiring_interviewing: {
          hours: hiring_interviewing_hours,
          description: hiring_interviewing_description,
        },
        delivery: {
          hours: delivery_hours,
          description: delivery_description,
        },
        appraisals: {
          hours: appraisals_hours,
          description: appraisals_description,
        },
        mentoring: {
          hours: mentoring_hours,
          description: mentoring_description,
        },
        meeting_customers: {
          hours: meeting_customers_hours,
          description: meeting_customers_description,
        },
        empid: employeeid,
        day: date,
        month: month,
      };
      const url = "http://localhost:3001/fill_timesheet";
      const option = {
        method: "POST",
        body: JSON.stringify(body_details),
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${jwt_token}`,
        },
      };
      const response = await fetch(url, option);
      const data = await response.json();
      if (data.status_code === 200) {
        this.timeSheetFillSuccess(data);
      } else {
        this.timeSheetFillFail(data);
      }
    }
  };

  resetAllButtonClicked = () => {
    this.setState({
      documentation_presentation_hours: "",
      documentation_presentation_description: "",
      training_learning_hours: "",
      training_learning_description: "",
      proposals_technical_hours: "",
      proposals_technical_description: "",
      hiring_interviewing_hours: "",
      hiring_interviewing_description: "",
      delivery_hours: "",
      delivery_description: "",
      appraisals_hours: "",
      appraisals_description: "",
      mentoring_hours: "",
      mentoring_description: "",
      meeting_customers_hours: "",
      meeting_customers_description: "",
      is_error: false,
      err_msg: "",
    });
  };

  render() {
    const {
      is_error,
      err_msg,
      documentation_presentation_hours,
      documentation_presentation_description,
      training_learning_description,
      training_learning_hours,
      mentoring_description,
      mentoring_hours,
      appraisals_description,
      appraisals_hours,
      delivery_description,
      delivery_hours,
      proposals_technical_description,
      proposals_technical_hours,
      hiring_interviewing_description,
      hiring_interviewing_hours,
      meeting_customers_description,
      meeting_customers_hours,
    } = this.state;
    const token = Cookies.get("jwt_token");
    const role = JSON.parse(token).role;
    if (role === "admin") {
      return <Redirect to="/not-found" />;
    }
    return (
      <div>
        <Header />
        <div className="d-flex flex-row">
          <Sidebar />
          <div>
            <div className="card2 m-2 mt-5 d-flex flex-column">
              <div className="d-flex flex-row card3">
                <div className="left">
                  <h6 className="mt-2 ml-2"> Task</h6>
                </div>
                <div className="center">
                  <h6 className="mt-2">Hours</h6>
                </div>
                <div className="right mt-2">
                  <h6 className="">Description</h6>
                </div>
              </div>
              <hr className="hr1" />
              <div className="d-flex flex-row ml-2">
                <h6 className="pr-5"> Documentation &amp; presentation</h6>
                <input
                  type="text"
                  className="text1 pr-3"
                  onChange={this.changeDocumenthours}
                  value={documentation_presentation_hours}
                />
                <input
                  type="text"
                  className="text2 pr-3"
                  onChange={this.changeDocumentdescrip}
                  value={documentation_presentation_description}
                />
              </div>
              <hr className="hr1" />
              <div className="d-flex flex-row ml-2">
                <h6 className="padding1"> Trainings learn &amp; Learning</h6>
                <input
                  type="text"
                  className="text1 pr-3"
                  onChange={this.changeTraininghours}
                  value={training_learning_hours}
                />
                <input
                  type="text"
                  className="text2 pr-3"
                  onChange={this.changeTrainingdescrip}
                  value={training_learning_description}
                />
              </div>
              <hr className="hr1" />
              <div className="d-flex flex-row ml-2">
                <h6 className="pr-5"> Proposals &amp; Technicalsolutions</h6>
                <input
                  type="text"
                  className="text1"
                  onChange={this.changeProposalhours}
                  value={proposals_technical_hours}
                />
                <input
                  type="text"
                  className="text2 pr-3"
                  onChange={this.changeProposaldescrip}
                  value={proposals_technical_description}
                />
              </div>
              <hr className="hr1" />
              <div className="d-flex flex-row ml-2">
                <h6 className="padding2"> Hiring and interviewing</h6>
                <input
                  type="text"
                  className="text1"
                  onChange={this.changeHiringhours}
                  value={hiring_interviewing_hours}
                />
                <input
                  type="text"
                  className="text2 pr-3"
                  onChange={this.changeHiringdescrip}
                  value={hiring_interviewing_description}
                />
              </div>
              <hr className="hr1" />
              <div className="d-flex flex-row ml-2">
                <h6 className="padding3"> delivery</h6>
                <input
                  type="text"
                  className="text1"
                  onChange={this.changeDeliveryhours}
                  value={delivery_hours}
                />
                <input
                  type="text"
                  className="text2 pr-3"
                  onChange={this.changeDeliverydescrip}
                  value={delivery_description}
                />
              </div>
              <hr className="hr1" />
              <div className="d-flex flex-row ml-2">
                <h6 className="padding5"> Appraisals</h6>
                <input
                  type="text"
                  className="text1"
                  onChange={this.changeAppraisalhours}
                  value={appraisals_hours}
                />
                <input
                  type="text"
                  className="text2 pr-3"
                  onChange={this.changeAppraisaldescrip}
                  value={appraisals_description}
                />
              </div>
              <hr className="hr1" />
              <div className="d-flex flex-row ml-2">
                <h6 className="padding5"> Mentoring</h6>
                <input
                  type="text"
                  className="text1"
                  onChange={this.changeMentoringhours}
                  value={mentoring_hours}
                />
                <input
                  type="text"
                  className="text2 pr-3"
                  onChange={this.changeMentoringdescrip}
                  value={mentoring_description}
                />
              </div>
              <hr className="hr1" />
              <div className="d-flex flex-row ml-2">
                <h6 className="padding4"> Meetings-Customer</h6>
                <input
                  type="text"
                  className="text1"
                  onChange={this.changeMeetinghours}
                  value={meeting_customers_hours}
                />
                <input
                  type="text"
                  className="text2 pr-3"
                  onChange={this.changeMeetingdescrip}
                  value={meeting_customers_description}
                />
              </div>

              <div className="d-flex justify-content-center">
                <button className="mr-4 btn4" onClick={this.submittingData}>
                  Save
                </button>
                <button className="btn4" onClick={this.resetAllButtonClicked}>
                  Reset All
                </button>
              </div>
              {is_error && <p className="para34 pb-5">* {err_msg}</p>}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Filltimesheet;
