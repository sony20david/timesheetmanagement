import { Link } from "react-router-dom";
import "./sidebar.css";
import Cookies from "js-cookie";

const Sidebar = () => {
  const token = Cookies.get("jwt_token");
  const role = JSON.parse(token).role;
  return (
    <div className="sidebar">
      <Link to="/">
        <div className="sidebar-header">
          <svg
            stroke="currentColor"
            fill="currentColor"
            strokeWidth={0}
            viewBox="0 0 24 24"
            height={24}
            width={24}
            xmlns="http://www.w3.org/2000/svg"
          >
            <path fill="none" d="M0 0h24v24H0V0z" />
            <path d="M18 4H6c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 14H6V6h12v12z" />
          </svg>
          <span>Company</span>
        </div>
      </Link>
      <div className="below">
        <div>
          <div className="sidebar-body">
            {role === "hr" || role === "employee" ? (
              <Link to="/profile">
                <div className="side1">
                  <img
                    src="https://img.icons8.com/ios-glyphs/60/000000/test-account.png"
                    className="icon-img"
                    alt="timesheet"
                  />
                  <span>Profile</span>
                </div>
              </Link>
            ) : null}
            {role === "hr" || role === "employee" ? (
              <Link to="/timesheet">
                <div className="side1">
                  <img
                    src="https://img.icons8.com/ios-filled/60/000000/property-time.png"
                    className="icon-img"
                    alt="timesheet"
                  />
                  <span>Timesheet</span>
                </div>
              </Link>
            ) : null}
            {role === "hr" || role === "admin" ? (
              <Link to="/allemployeesview">
                {role === "hr" ? (
                  <div className="side1">
                    <img
                      src="https://t3.ftcdn.net/jpg/02/27/25/12/360_F_227251234_0JnUIGzzksEC1az3PHJ8Folr6yoOJQ7A.jpg"
                      className="icon-img-1"
                      alt="timesheet"
                    />
                    <span>Hr view</span>
                  </div>
                ) : (
                  <div className="side1">
                    <img
                      src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAilBMVEX///8cHBwAAAASEhIaGhpNTU1XV1fKyspJSUn8/Pz39/cYGBg5OTkQEBCoqKjz8/MlJSUgICDf398ICAg6OjozMzNxcXE0NDQrKyvq6upoaGhfX18pKSnPz8/u7u50dHSfn5+9vb18fHybm5vBwcGvr6/X19eSkpKEhIRkZGRbW1tBQUGkpKSKioqhoWFVAAAJp0lEQVR4nO2dDVuyOhiAx7OhjoGAgOBHZpaaWf//751nw0xLg5nKeM/uLhQQvXazsW+IEIvFYrFYLBaLxWKxWCwWi8VisVgsFsu/BFd/ZLBcP3flyj/IysOlA8j63zRcFIS8Q8AcB0a4OXpajpoO0nVZwJCswZHAA3lxZVz+U4pLEL2nUtBhWQ8ovievTYfqanAyAuaIxNkhaPkOm8fN5mPWdPD+zoqT8d7uCJrQALZNh++PcOJB3IeTgruYfGo6jH+DYwbq/CbYfkUSiV/9MOeBtwEh7S0jn36PwPJ6hPGqpYb8gfhVUagI4KPpsF4CJy70aB1BmVS7TQf3EjCF1opBlVLzpkN7CWnNCFS0MUt9qJHJHERiC4v+xemazNlIfGk6wNokTMuQ9poOsC56idRRzap28axrGDw2HWRNOjo5qYS6TQdZD071LsP2XYgj3UTaOkPtjMYRftNh1uNF25Al7Wpf6MehA6umA62F/nXYtlqN988bkrR2y2lv2K5USsaBtmHLesG1a21MtCsv1S8u2lZrw8w01jNs3UBGqFvzhn7TQdajC7o177Y1EN8vKA/fmw60FssLDJdNB1qLOt353w3b1aF4Sc27Xdfh4ALDdtVpPM3OxPa1D0ndQZk9bevFIB96Xd5Yp/lo2SBiV7tHuGUDbJzEeheiiJsOsja/TsE4EYUtq5ZKtjpt4KCFo2tkRutnNglr5eSocFNXMRiHTQf2Qob1EmobB4B31OzKaF3z/ouwniG2KtpV2B+wqZNMWeI1Hc7LqdUQThZNB/MP1EqmMGg6mJfDyWN1gRGM23sVIrPKPjfWspbvDyo7bFrWAXWCj4o5wotWp1HF5td53m9NB+8KcPd8bpNsvfZHISqm58r9IG9xWf8FJ+GZAWGRz4yPQV4DlJifvhTlSEWdX1BnoYFTwftujMVZFeCdHRCGedmxWvELDovdJvo4Rj5QUelXbQg1foNR8O9eL/Dimt0wlYb1fiaI750pvdbtSruSoQN3biZ7Sc35MkmER6/PGK7xs6hmZ464cytyVYaZVgEdWSC4pwcxqIufzTpQ+Svl+bjvjKKy0566nQreD07HiUhUgX6v+pXyBN25218ZijpTYDgZnO3gZ/GgVjnnioYMg7eK8Kmyug/nL1mhOvN5xa+Qt6AhQ9qpPnAw+bUNzGBSow9DTYs307A7gapiM4BJZdBNNRw9F5V+pWPx/HuNxUhDvuxA7XlRFKCz/OViNMxQhvThEaD2QD6L0q2Pko8P5EwTwjBDMlpnoDEVg2VuNvV7DksgW59OreYYysbgE6ZOvRnQbpz20iJ1ZOEBnSf+s/Qwx5B479NamcthFBbbzKd+ti1PSwDT9x8VUFMMOVkmmtEnmfaKPJ2m+54OAcn3sShTDLH2kvmFVvwpq15RFL7vH+z+Pm3BDENOXkCkcaYj6OcyQ4p6214aH02a/mZjhiEJgUlDtouZfWYqHKbWmeqQ+dyvul+ylKmentgp38sjVEqdHaZTQwyHWZ75aMiyNI0dkWPCi5gs7HI/FzJHSX3MMXGnUsCP8ziTqTPFuMSMlMk9cpGyUTbk3pejGYYrJqa+NCwKx49EGsWR48dO5Atc8xmusIgVuMhiwckyVMckHUVRHjl5FrM8iuMIlziXxwpYeV9ZqhmGi2RbMAw0ptQ0wkgo8h7zY+EzP8ujLGK+kKVCqqLZQc20QJkUM5mpIw0dH89PMfXzwsdPRZotyNcMFCMMPWmBSpnwMbgsjzER5gXuwd0Yobh7Wojc8YUvI4mhDHPwOhR4LliMhlGWM+EXEZaMGPeYfIEbFocDB1MaBj+jpaGDV1QmpGGsMiDhR6UhigtHSEOZ0wghjdAwznK5N5ILbhdZdNBuNMJwRZMswQuJQhEFAA6bxlh0A2S4G4pgCoWIAV+hSAqslRcxVrUBE6qI8PBplATRVG5TtWSQxbAi3Au5OYY8DEPPwwXfQ75fUy9hOFMrPJx5uJR7PXWQXPV2B31uh7zc5t5sFppjiCdcwtU7bnreblu+YuDle7mE5Sdf35FvfLeUx5TfNs1QhZcot8+tvStXpnIfJ+U633+F775Smn5+3duJGpRKq+DH6/zUZ2db+UYZhptOZ7gL6scYGb72R7uG/3g4fi3nkXLSHeKG/KGOGjmbj8cL1fr1xp2fI2lGGfaB0l1YPEgCUJTTRzcQBDAh+88CORnqHT9F4wQPVbcjjNT2NwwylEMUwknKx5R4iSM6b64DLIll1Klbg6Ec2F7KXipp2IdgiN+TVW9149NIbX/DIEMMoHAdQVUy9RI1PMqX4CQy6sYBHdNd068TZG/00FDEQuSkBYZzgK5Ly1vrpaFKnk+gomscwAuUt8YMIFmPjwxpJ2VyepTxhj0K8mGzQ5kU94YkFfCsDFcdpUXWAIPJsaG7BBFx4w0xbsbyeaWgCsK94WsiL01puAQ1LMpoTo5TKU0JYzA33vAZZALFiJSZxpfhHIJNaRiCmMpvY5x2vhv28cxw0w1zSj0VapmzfBk+QzBWhg/kTb4ME5T7YUgKBs+h2YYroJ3BaiVzltmh4SKRM52V4RKSBVf5zbdU6ssciYmByYZczrQUsoQvZ41+GWZqJFQZhlgQ4sXYP2Uoc6SPxGBDQiJGy0qMegaENFSVNCwQ5URgZSin7wdq/wnDcmTZYMMHoO5ogLwopc8Svw8xyFpOaSgnDavL9IShmrRhsuFr8tlZjcnyXRqKnrsFzCJTWXqUhnKKkMpqTxnKWRsGG3KafM5KX0OylbXrpEy0C9Xm24C6IX0MgSouO3BY84aMlMcEBte8w8nk82m5g8kEm1Dr4ePj42L9tOsW7L9t5AlYbSZzuTl/2+DV+DCZyKnsi7e1OsZ7HY5/3pdviiEnR43YiokyvM5BO0wxPNL7pvsDfnCceudH28cYY3gzrOEtsIbXxRreAmt4XazhLbCG18Ua3gJreF2s4S2whtfFGt6C/4khi1669+AlYvc3LG9HY3Af1JzTO9+dx+v/L6crIZw738195rbQ2wHr+woSvtV9nPWfiGF75yjkxJtAElTe33odggQmTTxAo7twe/fBXbTswZiWJun2qm7ePnkv9s8RwZJh5b3gp+jdMsm+wEWZy5mHIPOLsqrgpv/MRPvZuSVBr3+KnvZ/+VDctPo2u7CcpycrY7qPi/40vOmzTieXnfZrEkxuKchnl574q0GPb/q6PoP4sszmSgTg3Pwpi9584jbHZH7zZ7iY/mwui8VisVgsFovFYrFYLBaLxWKxWCwWi8VisVgsFovFYrFYLBbL3/gPNE6w4dPODz4AAAAASUVORK5CYII="
                      className="icon-img-1"
                      alt="timesheet"
                    />
                    <span>admin view</span>
                  </div>
                )}
              </Link>
            ) : null}
            {role === "admin" ? (
              <Link to="/register">
                <div className="side1">
                  <img
                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOMAAADeCAMAAAD4tEcNAAAAhFBMVEX39/cAAAD////6+vr8/PzGxsbS0tKXl5f09PTCwsLj4+Pw8PDf39/r6+tkZGTX19dUVFRJSUmtra27u7sTExOIiIjOzs61tbWenp6Ojo55eXmTk5MrKyshISFtbW2kpKRcXFw3NzcYGBh/f38oKCg/Pz8zMzNEREQLCwtRUVFoaGgWFhb96sGEAAAJs0lEQVR4nO2daXviLBSGDRw1GqPWJUbbutRu0/n//+81agdCNuBASHxzf5vrsiOPEDgbJ71eR0dHR0dHRwcaCjcodT0SK1AgEM76m8VisenPQkrgwXQC6Q3i45vHWM2HkU/A9cCMARDtvr0cpiNKHmI2iR+/5wm88r0NiesBooHec6HAG+eg3Sop2VQoTIjbvP1AOJeQ6Hkfs9ZOJVlKKUw4tFQk2UpL9Lx9K9cr/ChI9Lwnv30iQe5RZHwGbROpLNHzTi2bSbJXlnhZrq0y7chQQ6Ln7Vq0u0JfS6LnLVojkgaaEj1v3JZHkqidGjxfLXkktVdqwqIdIuEVodHzXQ9fBlhgJHrPbdh2cNPYiokEeWcjn0Pzn0jyB6nxpfEa6Rgp0fOipp+RoGfF8ZybvuuQL7TG16Yv1hAt0fMmzV6sMDKgseE7q1IMJ5/X54bPIzkiFR77pOnpAfKCUribtCDNQzEKp5OmT+EVfe/Y+4xaoRBj5TxD81fpFTrRVPi3PRkPXY3TXv4kUkobVz6gqTHPL6ZASC+cRP3RqD8b+9CY+gGqZcptMhIvAsPl+WnFPnPaHyZNSaxrSByJEi8Chx85H3zbRU04PiG3tqGUviARyKg4V/IW+843J/KkKlGYRUqWFabS1rVKclaUKET/yUDC/1y43X5UA4/rlETaW0v91dPE5VQqHh5/UmMlE2mL3mnyRy26mkodK5RIXDwUh+uV7BQG2udPAsWcpcP6AZWETiqlSuQeRcbJXf2A/GL95qP+GkGST2czKT/YDbdSSaws0WH9gLTJ+sGtVM1w3t7V7iprBkRsFrRda1claJIT+cVPY54FLoWrMKXcEzli06hZ6ZJwcuaHrKoH98KmEZXqih2tVpkzkov34+LOrk5JiW0n/Dc0GmEkusvkQZWHNGUjI+rVgylCRxNJw4p4ADv/6Qwn0ds6m8gKH4v9+Mpetci3O7N1UDYu7nD0kRI9b+ns/ICyNcgCquhSF8/7cecvw7jYA2EGgFYxr4AziReRfuHBxx5HeCv6jDx9h0FXWuQxrdiuqpsE4nl2Glgmk9yqKxaqQhYQ3pi7DbhSssy5NMeCHOiT44rr0DnA4lMc05BtOdgaiSuBS4FXgPSF4mtWbIyskbgza0A+Cwj0zyc2JnZqa+SAcnC5sSYkudIw2gzPP+y4ZJWNeCsnYePy8AASRPE+YwswjYg6EA5nhfaU0FmcX7DLNJooIXSlkRIYnAttuUeYRyCTdVlMh9sjjGis3/MgsKkIA7A9AiTCW9XUXJ1NSTCsPA9YwIpoR1Z56o2yklAm/cSyx4irWRy1KvTlEjosYgVVHRJkeKnPXgU4yA6KPY8marPrS+2QSN72/GdE6xVmCdRVfw6+StBiwMJyBjadmrYcpXqF1MaKL7J/r2WpQk8x8sQ2HXQIuaYgMpkpB564Pz5hNdbhPRLZ7ZSDWXPSm3ERT/ankVKdCOnaXKDcvvMIgdbGuGLrC7nrvNuXONY0qlmkHNFNIMH6NBLtXfHIJSB1anN+Odl+GgmiEwDX0YEignMDy5sqwRibXLFceR6vFNuZcswseqkct/a282lXIebXv8JXPaqXod+wbKlWZcOVBkgDrVjy0u5KNeAU8YXWWr+Y9fqjTKJGHf5o01j5tjt7mMk38aU1pfUDeQxtSzQRhkkZAonJpOS92L4ZgOoCxJN6oqivUHE1sCzRSBDmRsRbm4X1AxmOgXUrFVnhxpM+4chM6qBcWL8kqOMTF7JOTwiFRaUjU0NTWgPdY355OWSHC/Twt+xvduMabnrq2l0Z5v38G5tAlkUn03sc1HGX1UhFzYWfkpYOQPzNXnS4VvN4UlOfCzNZ0Z/Clg7Xu9bjaBPv0iK/p9vFaOKTGu5eGykams8KFAKB8XI7L9l2TrvFrGdXp4n6tvd+rsKkQGK5K25lznE82Fy1ZIqWGOc2rQASHlQ2s9f1wJJMivSLPW+a11eeEn+hvlu/ba10okG7G8uc3x7ITOViKM/Thpq2B7AJmGmOmQmwxBy4q6HhExOZvd9kRwOwQTvba5OvVcD5G5/jrN2WW92qodKcBat8ZZhnl2kQQ0l0MqEwIaamdh/EILJuOwmN1K3cKTh0VcFcxYhEifL+sCx5G5oy+q7xayh+PRkbKbBKg3ed9f3GL/GSOzXqZjPQIRDtpPZUtKAhMBK5zENsxqOIbtfRfWadDowUkefzjFmvuks18w4AS+v0lyOi+4Om/5+VaOTWSgnvmQ1OGj07TizXo9Rg3LII7fgy6HzbUZQYnAzrySXT6UwOLc9RbBoCoRHztJpsxzqpadQwS16Exx9CZAt6ebQyPjqPo3CrnYYG7nPKolPTStRrjQZCmD+oQ+LrIbrSVz9BNFxHYblQE5nnSr56hN5Qn0b1jKN4MAL2PQJSzBAGq7KxKjZ9UepXpo++Qo0hCvuNcW+xAJRGxZUmHFDGkutVYDSC2oYhmHAGk+sV3L+PcMjvPkrftBIa94CplGUl95/0PP3HTr73zHqvMJOj9IZj4H6DLLefNOXpvkhrTLKCk/5iPZWQKjQJwRYPqnCdRiG3plTiSgGSBR7OlvF5+lHszAsrldZmpd41Cr+pzq1zetMKvUm0idc/Ty9CjYLYGhcTeVbFlMas2CAc9JeH7W769PEqnv4GLuK41JhWC7edWnwFl7EyENcaizHQ56jxGmV6BrZcY112qkuNZvpxNFqjoVreRmusexodaNQJ5rVOY72bqguNNZ+NTjTWa+I40WiwYLmxGq06HPEol7o1Uotx8ZBALjVrRPY5LqW093qNGm0mjEvvPdY5jxaXaunr6cX4kT2N+HLeao2UAM2S0UjyPmSi9MzAS4SrNNIxos5nbuCer1UD4KYR1xBzhddo5h5IATeNSK8G/fJ3u1mchmi0+Tg2RKOZC8tFNESjGTEFNEKj5YxjIzRaThw3Q6PdAs5GaLScrGqGRvz9ujKaodFC2T/H3V7F/Sfo53FnNV51t1dRdQZntMuV5NSjxXb/ZSXEevetyCAe5iA457u8z8SZWzVaJE5aknS9lQ8Y9Zh/fWSaF9MRm6VFubEfs3dgbxl1GoyTbvPTLxMVEKVxgPpzrNx3/5YPhJPBchGf939OunpLh+1SIxsEXz5wERyNNofDcL3bT+dfp/fSThYXvl8/T9vGaxSGRG9PFatwg57vB+GFMUfy78Dvwf0TZf9h8zQWkQkzSf9hezRq02l8DDqNj0Gn8THoND4GncbHoNP4GHQaH4P/g0Yxhe3qZd9WSRex224N7Qgynv0j20XrQdAJkXR0dHR0dHR0dHR0OOI/pLCe9jXn+EQAAAAASUVORK5CYII="
                    className="icon-img-1"
                    alt="timesheet"
                  />
                  <span>Create Profile</span>
                </div>
              </Link>
            ) : null}
            <a href="#/">
              <div className="side1">
                <img
                  src="https://img.icons8.com/ios-glyphs/60/000000/leave.png"
                  className="icon-img"
                  alt="timesheet"
                />
                <span>Requests</span>
              </div>
            </a>
            <a href="#/">
              <div className="side1">
                <img
                  src="https://img.icons8.com/ios-filled/60/000000/purchase-order.png"
                  className="icon-img"
                  alt="timesheet"
                />
                <span>Payslips</span>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
