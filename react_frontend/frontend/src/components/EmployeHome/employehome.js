//importing component from reack package
import { Component } from "react";
//importing css properties from employeehome.css file
import "./employehome.css";
//importing header component
import Header from "../Header/header";
//importing sidebar component
import Sidebar from "../Sidebar/sidebar";
//importing cookies from js-cookie package
import Cookies from "js-cookie";
//importing redirect from react-router-dom package
import { Redirect } from "react-router-dom";
import { setProfileData } from "../../redux/actions/authenticationActions";
import store from "../../redux/store";

/**
 *Inheritance uses the EmployeeHome extends to allow any component
 *to use the properties and methods of another component connected with the parent.
 */
class EmployeHome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //setting state employedata with empty array
      employeData: [],
    };
    store.subscribe(() => {
      this.setState({
        employeData: store.getState().employeeProfile.profileData,
      });
    });
  }
  //componentDidMount() is invoked immediately after a component is mounted.
  componentDidMount() {
    //it calls the apicall function
    this.apicall();
  }

  //It is an asynchronus call to get "jwt_token",fetching data
  apicall = async () => {
    const token = Cookies.get("jwt_token");
    //Parse a string (written in JSON format) and return a JavaScript object
    const cookies_data = JSON.parse(token);
    //intializing url with path,option method "get",accepting headers content-type json
    const url = `http://localhost:3001/get_specific_emp/${cookies_data.emp_id}/${cookies_data.role}`;
    const option = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${cookies_data.jwt_token}`,
      },
    };

    /**
     *It is an asynchronus function to handle fetch call
     *it sets State of employee data like name,date of joining etc..
     *Storing the information in localstorage "lastname",converting json data to string.
     */
    const resonse = await fetch(url, option);
    const data = await resonse.json();
    store.dispatch(setProfileData(data[0]));
    //setting state with employee data
    //setting item of employee data in localstorage
  };
  //getting jwt_token from cookies.
  render() {
    const { employeData } = this.state;
    const token = Cookies.get("jwt_token");
    //Parse a string (written in JSON format) and return a JavaScript object
    const role = JSON.parse(token).role;
    //if role is admin it will redirect to not found page
    if (role === "admin") {
      return <Redirect to="/not-found" />;
    }
    //redndering Employeehome component
    return (
      <>
        <Header />
        <div className="d-flex flex-row">
          <Sidebar />
          <div className="div1">
            <div className="employee-card m-4">
              <h1 className="image">M</h1>
              {/**
               * setting State of employee name with employeData.name
               */}
              <h4 className="">Employee Name : {employeData.name}</h4>
              <h4 className="employee-id">
                {/**
                 * setting State of employee empid with employeData.empid
                 */}
                <span className="values join-year-value">emp-ID : </span>
                {employeData.empid}
              </h4>
              <div className="employee-details">
                <p className="details">
                  Year of Joining :
                  {/**
                   * setting State of join_year name with employeData.yearofjoining
                   */}
                  <span className="values join-year-value">
                    {employeData.yearofjoining}
                  </span>
                </p>
                <p className="details">
                  Role :
                  {/**
                   * setting State of employee job-type with employeData.role
                   */}
                  <span className="values job-type"> {employeData.role}</span>
                </p>
                <p className="details reg-year">
                  email :
                  {/**
                   * setting State of employee email with employeData.email
                   */}
                  <span className="values job"> {employeData.email}</span>
                </p>
                <p className="details reg-year">
                  {/**
                   * setting State of employee phoneno with employeData.phoneno
                   */}
                  Phone no :{" "}
                  <span className="values"> {employeData.phoneno}</span>
                </p>
              </div>
            </div>
            <div />
          </div>
        </div>
      </>
    );
  }
}
//exporting employeHome component
export default EmployeHome;
