//importing React,component from react package
import React, { Component } from "react";
//importing Redirect from react-router-dom package
import { Redirect } from "react-router-dom";
//importing Cookies from js-cookie package
import Cookies from "js-cookie";
//importing css properties from admin.css file
import "./admin.css";

/**
 *Inheritance uses the Admin extends to allow any component
 *to use the properties and methods of another component connected with the parent.
 *getting "jwt-token" from cookies if jwtToken is not undefined then redirect to register page
 */
class Admin extends Component {
  //setting state with username,password,is_login,err_msg,jwt_token with empty
  state = {
    username: "",
    password: "",
    is_login: false,
    err_msg: "",
    jwt_token: "",
  };

  //It is an asynchronus function which will occur any event happens
  changeUsername = (event) => {
    //setting state with username=>that has taken by the user
    this.setState({ username: event.target.value });
  };

  //It is an asynchronus function which will occur any event happens
  changePassword = (event) => {
    //setting state with password=>that has taken by the user
    this.setState({ password: event.target.value });
  };
  //It ia an asynchronus function to handle loginSuccess
  loginSuccess = (data) => {
    this.setState({ is_login: false, err_msg: "" });
    const { history } = this.props;
    //setting jwt-token
    Cookies.set("jwt_token", JSON.stringify(data), {
      expires: 30,
      path: "/",
    });
    //if everything satisfies condition goes to register page to regsiter employee/hr.
    history.push("/register");
  };

  //It is an asynchronus function to handle if status_code is not 200
  //it gives error message if login fails
  loginFailure = (event) => {
    this.setState({ is_login: true, err_msg: event.status_message });
  };

  //this is an asynchronus function to handle the submitting data
  submitingLoginData = async (event) => {
    //if any event happens without entering the data then preventDefault will take care of that
    event.preventDefault();
    //setting state with entered username,password by user
    const { username, password } = this.state;
    const userDetails = {
      username,
      password,
    };
    //setting path to url,method type
    const url = "http://localhost:3001/login/admin";
    const option = {
      method: "POST",
      //converting json data into string type,headers will accept content-type:json
      body: JSON.stringify(userDetails),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    /**
     *fetching url,option if data.status_code is 200 then goes to loginSuccess function
     *else it goes to loginFailure function
     */
    const response = await fetch(url, option);
    const data = await response.json();
    if (data.status_code === 200) {
      //if status_code is 200 then goes to loginsuccess
      this.loginSuccess(data);
    } else {
      //if status_code is not 200 then goes to loginfailure
      this.loginFailure(data);
    }
  };
  //getting "jwt-token" from cookies if jwttoken is not equal to undefined goes to home
  //setting state with username,password,is_login,err_msg
  render() {
    const { username, password, is_login, err_msg } = this.state;
    const jwtToken = Cookies.get("jwt_token");
    if (jwtToken !== undefined) {
      return <Redirect to="/" />;
    }
    //rendering admin login component
    return (
      <div className="form-row">
        <div className="login-form-container card2-imp">
          <img
            src="http://eurekawebsolutions.com/wp-content/uploads/2017/06/Admin-Support-NJs-1.png"
            class="logo1"
            alt="website"
          />
        </div>
        <div className="card1-imp">
          <div className="panel-admin-login position-relative">
            <div className="py-4 px-4 rounded-top border-bottom text-white">
              <h1 className="heading text-center">Admin Login</h1>
            </div>
            {/**
             * on clicking submit button it goes to submitingLoginData function to verify details
             */}
            <form className="p-4" onSubmit={this.submitingLoginData}>
              <div className="form-group ">
                <label htmlFor="exampleInputEmail1">Username</label>
                <input
                  //ontyping the username it goes to changeUsername function
                  onChange={this.changeUsername}
                  className="form-control form-control-lg "
                  placeholder="Enter username"
                  value={username}
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputPassword1">Password</label>
                <input
                  //ontyping password it goes to changePassword function
                  onChange={this.changePassword}
                  type="password"
                  className="form-control form-control-lg"
                  id="exampleInputPassword1"
                  placeholder="Password"
                  value={password}
                />
              </div>
              <div className="row">
                <div className="col-6">
                  <div
                    className="form-group form-check"
                    style={{ display: "none" }}
                  >
                    <input
                      type="checkbox"
                      className="form-check-input"
                      id="exampleCheck1"
                    />
                    <label className="form-check-label" htmlFor="exampleCheck1">
                      Check me out
                    </label>
                  </div>
                </div>
              </div>
              <div className="d-block text-center">
                <button type="submit" className=" button1 badge-pill px-4">
                  Login
                </button>
                {/**
                 * It shows the login success or login fails message based on true or false
                 */}
                {is_login && <p className="para1">* {err_msg}</p>}
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

//exporting Admin component
export default Admin;
