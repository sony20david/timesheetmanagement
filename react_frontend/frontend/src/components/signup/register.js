//importing react from react package
import React from "react";
//importing css file from register
import "./register.css";
//importing container,form,button from react-bootstrap
import { Form, Button } from "react-bootstrap";
//importing useState from react
import { useState } from "react";
import Header from "../Header/header";
import Sidebar from "../Sidebar/sidebar";
import Cookies from "js-cookie";
import { Redirect } from "react-router-dom";

/**
 * It is functional based component
 * function to register the details of employee/hr
 * useState=>It helps us easily re-render our component,
 * It preserves the value of a variable between renders.
 * registering employee/hr with name,username,password,empid,yearofjoining,phoneno,role,email,
 * security_question etc.
 * setting useState empty first after filling the values in textbox it will go to the setState
 * and updates the value.
 */
const Register = () => {
  const [name, setName] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [empid, setEmpid] = useState("");
  const [yearofjoining, setYearofJoining] = useState("");
  const [phoneno, setPhoneno] = useState("");
  const [role, setRole] = useState("");
  const [email, setEmail] = useState("");
  const [security_question, setSecurityQuestion] = useState("");
  const [city, setCity] = useState("");
  const [district, setDistrict] = useState("");
  const [state, setState] = useState("");
  const [country, setCountry] = useState("");
  const [bloodgroup, setBloodgroup] = useState("");
  const [position, setPosition] = useState("");
  const [gender, setGender] = useState("");
  const [is_register, setRegister] = useState(false);
  const [err_msg, setErrorMsg] = useState("");

  /**
   * registerhandler is an asynchronus function it will check for all the fields are entered successfully
   * registersuccess  =>if all encoutered successfully then gives alert "employee/hr registered successfully"
   * resgiterFail  =>if any empty field occured then it will go to function registerFail and sets error message.
   * defining url path which is related to backend,options like method type,content-type
   * fetch() method takes the URL of the API,options as an arguments
   * this is an asynchronus function if every condition satisfies status_code is goes to resgistersuccess
   * otherwise goes to the registerFail
   */
  const registerhandler = async () => {
    //const user = {name,username,password,empid,yearofjoining,phoneno,role,email,security_question,city,district,state,country,bloodgroup,position,gender}
    //if gives alert message that employee/hr registered successfully
    const registerSuccess = (data) => {
      setRegister(false);
      setErrorMsg("");
      alert("Employee Register Successfully");
    };
    //it sets ErrorMessage
    const registerFail = (event) => {
      setRegister(true);
      setErrorMsg(event.status_message);
    };
    setCountry("india");
    const url = "http://localhost:3001/user_register";
    const option = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        // "Authorization":"Bearer "+localStorage.getItem("jwt")
      },
      //Convert a JavaScript object into a string.
      body: JSON.stringify({
        name,
        username,
        password,
        empid: parseInt(empid),
        yearofjoining: parseInt(yearofjoining),
        phoneno: parseInt(phoneno),
        role,
        email,
        security_question,
        city,
        district,
        state,
        country,
        bloodgroup,
        position,
        gender,
      }),
    };
    //fetching url,option
    //if status_code is 200 then goes to registerSuccess function otherwise resgiterfail function
    const response = await fetch(url, option);
    const data = await response.json();
    if (data.status_code === 200) {
      registerSuccess(data);
    } else {
      registerFail(data);
    }
  };
  const token = Cookies.get("jwt_token");
  const check = JSON.stringify(token).role;
  if (check === "admin") {
    return <Redirect to="/not-found" />;
  }
  return (
    <div>
      <Header />
      <div className="div1">
        <Sidebar />
        <div className="div2">
          <img src="/sign-up.png" className="image2" alt="mahesh" />
          <h1 className="mt-2 mb-4">Create Profile</h1>
          <Form>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>name</Form.Label>
              <Form.Control
                type="text"
                placeholder="name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>username</Form.Label>
              <Form.Control
                type="text"
                placeholder="username"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>empid</Form.Label>
              <Form.Control
                type="text"
                placeholder="empid"
                value={empid}
                onChange={(e) => setEmpid(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Year of joining</Form.Label>
              <Form.Control
                type="text"
                placeholder="year of joining"
                value={yearofjoining}
                onChange={(e) => setYearofJoining(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>phoneno</Form.Label>
              <Form.Control
                type="text"
                placeholder="Phoneno"
                value={phoneno}
                onChange={(e) => setPhoneno(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Role</Form.Label>
              <Form.Control
                type="text"
                placeholder="Role"
                value={role}
                onChange={(e) => setRole(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>security_question</Form.Label>
              <Form.Control
                type="text"
                placeholder="Security question"
                value={security_question}
                onChange={(e) => setSecurityQuestion(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>city</Form.Label>
              <Form.Control
                type="textarea"
                placeholder="city"
                value={city}
                onChange={(e) => setCity(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>district</Form.Label>
              <Form.Control
                type="textarea"
                placeholder="district"
                value={district}
                onChange={(e) => setDistrict(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>state</Form.Label>
              <Form.Control
                type="textarea"
                placeholder="state"
                value={state}
                onChange={(e) => setState(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>country</Form.Label>
              <Form.Control
                type="textarea"
                placeholder="country"
                value={country}
                onChange={(e) => setCountry(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>bloodgroup</Form.Label>
              <Form.Control
                type="textarea"
                placeholder="address"
                value={bloodgroup}
                onChange={(e) => setBloodgroup(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>position</Form.Label>
              <Form.Control
                type="text"
                placeholder="position"
                value={position}
                onChange={(e) => setPosition(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>gender</Form.Label>
              <Form.Control
                type="text"
                placeholder="gender"
                value={gender}
                onChange={(e) => setGender(e.target.value)}
              />
            </Form.Group>

            <Button
              variant="primary"
              className="mb-2"
              onClick={registerhandler}
            >
              Register
            </Button>
            {is_register && <p className="paragraph mb-4">* {err_msg}</p>}
          </Form>
        </div>
      </div>
    </div>
  );
};

//exporting component Register
export default Register;
