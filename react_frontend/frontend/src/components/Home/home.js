import { Component } from "react";
import "./home.css";
import Header from "../Header/header";
import Sidebar from "../Sidebar/sidebar";
import Cookies from "js-cookie";

function Home() {
  return (
    <>
      <Header />
      <div className="d-flex flex-row">
        <Sidebar />
        <div className="just">
          <div>
            <h3>Software Products and Digital Engineering</h3>
            Modernize and automate systems and processes, build and launch new
            products faster and optimize development costs; achieve scale and
            agility-leverage our low-code platforms, cloud engineering, and
            microservices architecture
          </div>
          <div>
            <h3>Client success</h3>
            Global construction tools company increases tools usage by 60% with
            tracking software solution from Innominds Innominds builds a
            tracking software that improves tool utilization, and creates a
            building maintenance management solution that results in higher
            customer satisfaction.
          </div>
          <div>
            <h3>
              Accelerate your go-to-market strategy and step up your innovation
              pace
            </h3>
            Leverage iSymphonyTM - our low-code platform and accelerators with
            an intuitive drag-and-drop UI-for rapid application development with
            high-quality and compliable code
          </div>
        </div>
      </div>
    </>
  );
}
export default Home;
